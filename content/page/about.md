---
title: "About me"
subtitle: "Brief intro and resource list"
date: 2018-11-20T15:10:10+01:00
---

A math student and a GNU/Linux user, built @staticmanlab on [GitLab][gl] and
[GitHub][gh].

### Useful math links

1. [My Math Ebook list][ebooks]
1. [Solution to some classical math books][math_sol]
1. [$\LaTeX$ cheatsheet (in French)][latex_am_fr]
1. [TeXit cheatsheet][texit_cheatsheet]
1. [My little online math + Markdown editor with instant preview][my_math_editor]
1. [Pandoc's demo HTML slides][pandoc_slides] (final slide) and
[its Markdown source][pandoc_slides_src]

### Useful tech links

1. [Google Advanced Search Form - Google Guide][search]
1. [Online Cangjie Input][canjie] (and others like Pinyin, Quick, and Jyutping)
1. [MATLAB--Python--Julia cheatsheet --- Cheatsheets by QuantEcon
documentation][matlab_julia]
1. [R vs Julia cheatsheet - datascience-enthusiast.com][r_julia]
1. [BitDegree's cheatsheets][bitdegree_cheatsheets]
1. [Online compilers and terminals on Tutorials Point][ide]
1. [Web development Support tables][caniuse]

### My online graphs

1. [My online Geogebra plots][geogebra_profile]
1. [Trigonometric approximation of sawtooth wave][graph_sawtooth]
1. [Simple sine functions for limits of integrals][sine_leb]
1. [My "recipes"][recipes]

### My presentations

1. [GraphQL with Ahmed][graphql_present]
1. [Introduction to functions for arts stream students][fct_intro]
1. [My SVG workbook][my_svg_wb]
1. [Note pour la 2e journée][note_2e_journee]
1. [Note pour la 3e journée][note_3e_journee]
([English version][note_3e_journee_en])
1. [Exos pour la 5e journée][exos_5e_journee]
1. [Exos pour la 1e journée en algorithmique][algo1]

### Games
1. [My tweak on Spaceball Cadet][my1stwebgame]'s GitHub Page port.

[gl]: https://gitlab.com/staticmanlab
[gh]: https://github.com/staticmanlab
[ebooks]: https://docs.google.com/document/d/13eZpSdn8pWT0WqkrEaxXOLZOxq5VgKmRWjoyz7NT97E/edit?usp=share_link
[math_sol]: http://homepage.ntu.edu.tw/~d04221001/Notes/Problems%20and%20Solutions%20Section/Problems%20and%20Solutions.html
[my_math_editor]: https://vincenttam.gitlab.io/math-live-preview/
[texit_cheatsheet]: https://wysc.us.to/docs/assets/texit_cheatsheet_1.pdf
[latex_am_fr]: http://www.edu.upmc.fr/c2i/ressources/latex/aide-memoire.pdf
[pandoc_slides]: https://pandoc.org/demo/example16d.html
[pandoc_slides_src]: https://pandoc.org/demo/SLIDES
[search]: https://classic.googleguide.com/sharpening_queries.html
[canjie]: https://www.cangjieinput.com/
[matlab_julia]: https://cheatsheets.quantecon.org/
[r_julia]: https://datascience-enthusiast.com/R/R_Julia_cheat_sheet.html
[bitdegree_cheatsheets]: https://www.bitdegree.org/learn/
[ide]: https://www.tutorialspoint.com/codingground.htm
[caniuse]: https://caniuse.com/
[geogebra_profile]: https://www.geogebra.org/u/vincentkltam
[graph_sawtooth]: https://www.desmos.com/calculator/oa2fn5gdc6
[sine_leb]: https://www.desmos.com/calculator/hkvzpvcbhh
[recipes]: https://docs.google.com/document/d/1pcbdaLVLTqPaFyxh7od3RKPt1a3THdN00SGUyiaHxB0/
[graphql_present]: https://vincenttam.gitlab.io/presentation-de-graphql/
[fct_intro]: /post/2022-09-05-functions-for-arts/fct-slides.html
[my_svg_wb]: https://vincenttam.gitlab.io/svg-ultimate-course/
[note_2e_journee]: https://vincenttam.gitlab.io/note-2e-journee/
[note_3e_journee]: https://vincenttam.gitlab.io/html-slides-exemplaire/
[note_3e_journee_en]: https://vincenttam.gitlab.io/html-slides-exemplaire/index-en.html
[exos_5e_journee]: https://vincenttam.gitlab.io/exo-3e-jour/
[algo1]: https://vincenttam.gitlab.io/algo1/
[my1stwebgame]: https://vincenttam.github.io/SpaceCadetPinball/

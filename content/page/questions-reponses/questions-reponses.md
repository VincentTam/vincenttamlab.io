---
title: "Questions Réponses"
subtitle: "Collection de mes Q–Rs"
type: page
draft: false
---

Une question ?  Une réponse.

- [<i class="fab fa-java fa-lg"></i> Java][qr_java]

[qr_java]: ../java/

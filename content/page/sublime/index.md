---
title: "Sublime Text 3"
subtitle: "An overview of my Sublime Text 3 Config"
date: 2018-09-05T20:52:51+02:00
type: page
toc: true
draft: false
---

### <i class="far fa-keyboard fa-fw" aria-hidden></i> Usual keys

I'll only list those I find useful.  See
[_Sublime Text Unofficial Documentation_'s list][key_list] for a more
comprehensive list.

#### <i class="fas fa-arrows-alt fa-fw" aria-hidden></i> Navigation

- `<C-Up>` / `<C-Down>`: move up ↑ / down ↓ _without_ changing
<i class="fas fa-i-cursor" aria-hidden></i> cursor position.
- `<C-g>`: go to line number
- `<C-p>`: go to other files
- `<C-S-p>`: show [Command Palette][stcsp]

#### <i class="fas fa-search fa-fw" aria-hidden></i> Searching / Replacing

As usual, it supports custom selection range and regular expressions as
expected.  Transformations preserving cases are possible.

- `<F3>`: jump to next matching word

#### <i class="far fa-edit fa-fw" aria-hidden></i> Editing / Selection

- `<F6>`: toggle spell check
- `<C-F6>` / `<S-C-F6>`: next / previous spelling mistake
- `<C-Enter>` / `<C-S-Enter>`: insert empty new line below / above current line.
Works like `o` / `O` in [Vim].
- `<C-S-Up>` / `<C-S-Down>`: swap current line up ↑ / down ↓
- `<C-S-d>`: duplicate current line
- `<C-S-k>`: kill current line
- `<C-KK>`: kill from cursor to EOL, like `D` in [Vim]
- `<C-l>`: repeat select line and move cursor to the beginning of next line
- `<C-[>` / `<C-]>`: unindent / indent current line, like `<<` / `>>` in [Vim]
- `<C-j>`: join current line with the the next line, like `J` in [Vim]
- `<C-/>`: toggle comment
- `<C-y>`: repeat last keyboard shortcut, like `.` in [Vim]
- `<Alt-q>`: Hard wrap line according to ruler settings, like `gq` in [Vim]

Multi-cursors:

- `<Alt-S-Up>` / `<Alt-S-Down>`: column selection up / down.
- `<Alt-F3>`: select _all_ instances of word under cursor.
- `<C-d>`: repeat select word, useful when used multiple times or with `<C-f>`
- `<C-S-l>`: select highlighted lines and move cursors to the end of lines.  Use
`<Esc>` to undo.

#### <i class="far fa-folder-open fa-fw" aria-hidden></i> Folder sidebar

- `<C-k><C-b>`: toggle folder sidebar (thanks to
[Itay K <i class="fab fa-stack-overflow" aria-hidden></i>][so21882826].)

### <i class="fas fa-user-cog fa-fw" aria-hidden></i> User config

The _User Settings_ file, written in JSON, can be opened with **Sublime Text >
Preferences > Settings** -- User.  After changing the file, _restart_ it to
apply the changed settings.

#### <i class="fas fa-indent fa-fw" aria-hidden></i> Expand tabs to spaces

At the right-bottom corner, there's a "Spaces: [num]", where "[num]" represents
your tab width.  Click on that to open a popup menu.  You may click "**Convert
indentation to Spaces/Tabs**" at the bottom.  The appropriate choice at the top
will be automatically made.

To make this preference persistent, you may set `"translate_tabs_to_spaces":
true`.

<i class="fas fa-info-circle" aria-hidden></i> Tabs should be _avoided_ in
source code <i class="far fa-file-code" aria-hidden></i>  and text files <i
class="far fa-file-alt" aria-hidden></i> unless necessary.  (say, [Makefile])

<i class="fas fa-external-link-alt fa-fw" aria-hidden></i> Further reading:

1. [Changing Between Spaces and Tabs in Sublime Text][css-tricks]
2. [Gnulib manual § 1.4.2][gnu-no-tab]

#### ↵ Ensure an empty line at end of file

To avoid [_missing newline at EOL_][post_eol], one can add one simple line to
the user preferences.

{{< highlight json >}}
"ensure_newline_at_eof_on_save": true
{{</ highlight >}}

#### <i class="fas fa-ruler-vertical fa-fw" aria-hidden></i> Set vertical ruler

Thanks to
[Ross Allen  <i class="fab fa-stack-overflow" aria-hidden></i>][so9910143], it
can be done by adding `"rulers": [80]` in the User Settings.

### <i class="fas fa-puzzle-piece fa-fw" aria-hidden></i> Plugins

Press `<C-S-p>` and type **Package Control** to see a list of available options.

#### <i class="fab fa-fonticons-fi fa-fw" aria-hidden></i> Icon Fonts

<i class="fas fa-external-link-alt fa-fw" aria-hidden></i>
[Icon Fonts -- Package Control][icon_fonts]

Sometimes useful.  I can get _some_ of the shortcut prefixes only.

#### <i class="fab fa-markdown fa-fw" aria-hidden></i> Markdown Extended

<i class="fas fa-external-link-alt fa-fw" aria-hidden></i>
[Markdown Extended -- Package Control][mdx]

Enables _some_ HTML-like autocompletion in [Markdown][md] and syntax
highlighting, say

- `p` → `<p></p>` for paragraphs
- `sp`  → `<span></span>` for `span`
- `t` → `<table></table>` for tables
- `/` → complete tag

[key_list]: https://docs.sublimetext.info/en/latest/reference/keyboard_shortcuts_win.html
[stcsp]: https://docs.sublimetext.info/en/latest/extensibility/command_palette.html
[Vim]: https://www.vim.org
[css-tricks]: https://css-tricks.com/changing-spaces-tabs-sublime-text/
[Makefile]: https://www.gnu.org/software/make/manual/make.html#Rule-Introduction
[gnu-no-tab]: https://www.gnu.org/software/gnulib/manual/html_node/Indent-with-spaces-not-TABs.html
[post_eol]: /post/2018-09-05-missing-newline-at-eol/
[icon_fonts]: https://packagecontrol.io/packages/Icon%20Fonts
[mdx]: https://packagecontrol.io/packages/Markdown%20Extended
[md]: https://daringfireball.net/projects/markdown/
[so9910143]: https://stackoverflow.com/q/9910143/3184351
[so21882826]: https://stackoverflow.com/a/21882826/3184351

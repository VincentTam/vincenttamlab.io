---
title: "Right PATH to Linux"
subtitle: "Roles of various config files"
date: 2019-05-18T11:25:57+02:00
categories:
- technical support
tags:
- Linux
toc: true
draft: false
---

### Background

#### Updated $\LaTeX$ version

From [this $\TeX$-SE question about tlmgr][345935], we see some advantages of
installing [$\TeX$Live][texlive] directly from the official site:

1. avoid errors due to outdated version of [$\TeX$Live][texlive] supplied by
the OS's package manager
2. easier to manage packages with `tlmgr`.
3. enjoy the newer version of [$\TeX$Live][texlive] _not_ yet available in your
current GNU/Linux version.

The installation took about 30 minutes and 5G in the disk.  I've chosen a local
installation as this _didn't_ require `sudo` privileges.  The whole process went
smooth and the system displayed a message about setting 

#### Atom & $\LaTeX$

Due to [Juno], an IDE for [Julia] on [Atom], I've started using [Atom] for
[$\LaTeX$][latex] document preparation.  I've chosen James Yu's
[Atom-$\LaTeX$][atltx] for the task.  Unluckily, I received an error during a
compilation because [Atom] _couldn't_ detect the locally installed
[$\TeX$Live][texlive] and the `$PATH` in [my ZSHRC][myzshrc].

### Problem

The problems in the previous section bring us to the classical problem of
setting the environment `$PATH` on GNU/Linux.  At first, I thought it was just
another easy piece of config work.  Either one of the following would do.

1. system-wide:
    + edit `/etc/environment`; or
    + create a symbolic link under `/usr/bin`, or `/usr/local/bin`, or etc
2. local:
    + edit my BASHRC/ZSHRC; or
    + create a symbolic link under `~/bin` or `~/.local/bin`, or etc

However, I struggled with these these for a day _without_ success.

### Discussion

Hours of experiments lead me to this (over-)simplified division of roles of
the system's config files.

Files | Scope | GUI | TTY | TERM | Roles
--- | --- | --- | --- | --- | ---
`/etc/environment` | system | ✓ | ✓ | ✓ | set environment `$PATH` system-wide
`~/.zshrc` | user | ❌ | ✓ | ✓ | set shell `$PATH` after `~/.zprofile`
`~/.zprofile` | user | ❌ | ✓ | ❌ | fix session `$PATH` right after shell login
`~/.xprofile` | user | ✓ | ❌ | ✓ | fix session `$PATH` right after GUI login (XFCE only)
`~/.pam_environment` | user | ✓ | ❌ | ✓ | set `$PATH` after GUI login (special syntax)

Remarks: You may replace "Zsh" with "bash" in the above table.  When
`~/.zprofile` (or `~/.bash_profile`) is present, the system would read it
instead of `~/.profile`.

- TTY: shell login in `/dev/tty{n}`
- TERM: terminal emulator in GUI

The difference between `~/.zshrc` and `~/.zprofile` is that the later is only
read once after login, while the former can be updated by either
`source ~/.zshrc` or starting a new terminal window in GUI.  Therefore, the
later holds shell varaibles that remain _constant_ throughout the session.

### Solution

Here's how I set the `$PATH`.

- system-wide installed [Julia] under `/opt`
    + `/etc/environment`
- locally installed [$\TeX$Live][texlive] under `~/.local/texlive`:
    + `~/.pam_environment`: I _couldn't_ find a [GNOME] counterpart of
    `~/.xprofile`.
    + `~/.zshrc`: larger coverage than `~/.zprofile`, more commonly set.

Since I [manage my dotfiles with GNU Stow][pp], after creating a new file, I
will `stow {folder}` to create the corresponding symbolic link(s).

[Juno]: https://junolab.org/
[Julia]: https://julialang.org/
[Atom]: https://atom.io/
[latex]: https://www.latex-project.org/
[atltx]: https://atom.io/packages/atom-latex
[345935]: https://tex.stackexchange.com/q/345935/126386
[texlive]: https://tug.org/texlive/
[myzshrc]: https://gitlab.com/VincentTam/dotfiles/blob/fujitsu/zsh/.zshrc
[GNOME]: https://www.gnome.org/
[pp]: /post/2018-07-08-stow-your-dotfiles/

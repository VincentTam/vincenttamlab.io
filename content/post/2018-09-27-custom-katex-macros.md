---
title: Custom $\KaTeX$ Macros
subtitle: More efficient math editing
date: 2018-09-27T19:32:58+02:00
type: post
categories:
- blogging
tags:
- Hugo
- KaTeX
- math
draft: false
---

### Background

Same as [the last section in _Beautiful Hugo Improvements_][pp1].

### Goal

To write math efficiently by automatically loading longer code with shorter
macro code.

For example, when I wrote [_Some Infinite Cardinality Identities_][pp2], it
would be ten times more quicker and efficient to type `\card{C}` than to write
`\mathop{\mathrm{card}}(C)` all the time.

### Changes committed to my <i class="fab fa-git" aria-hidden></i> repo
The current version of [Beautiful Hugo][bh] is still using $\KaTeX$&nbsp;v0.7,
which _doesn't_ support macros in auto-rendering.  It would be _inconvenient_ to
include the macros after invoking $\KaTeX$'s `render` function.

I spent a night trying to put [my custom $\KaTeX$ macros][mygl_macros] into
`layouts/partials/footer_custom.html`.  _Unaware of the $\KaTeX$ version_, I
_couldn't_ find a solution from [Google].

- Migrated the JavaScript code from the big `<script>` tag in
  `layouts/partials/footer_custom.html` to `static/js/katex-macros.js`
- Wrapped the auto render function `renderMathInElement()` with a listener for
  the `DOMContentLoaded`.

### Comparison with my MathJax macros

Once I had confirmed that $\KaTeX$'s support for equation tagging and custom
macros worked on this site, I ported [my MathJax macros][mygh_macros] to here.

[Hugo] interpreted `(C)` as (C), even inside a pair of `$...$`: `$(C)` becomes
$(C)$.

In [MathJax macros][mj_macros], one has to specify the total number of
argument(s) for macros that accepts argument(s).  As a result, I _didn't_
configure argument for operators for like "vol", "Int", etc.  I would type
"\vol(E)" on [my old Octopress blog][old_blog] for convenience.

However, to avoid typos like "$\mathop{\mathrm{card}}(C)$", I'm going to rewrite
my $\KaTeX$ macros so that they will be capable of accepting arguments.  This
will be carried out _gradually_ as I'm currently quite _occupied with my studies
in math_.

Some macros Qike `\N`, `\R`, `\Z`, etc, have been removed as they were _already
[supported by $\KaTeX$][10ptb]_.

### TODO list

- [x] enable $\KaTeX$ auto-rendering
- [x] port existing macros from my old blog
- [x] write some math articles to test the macros
- [ ] rewrite my macros' syntax to accept arguments
- [ ] promote $\KaTeX$ to the mathematical community
- [ ] watch $\KaTeX$ update for automatic equation referencing support

[pp1]: /post/2018-09-12-beautiful-hugo-improvements/
[pp2]: /post/2018-09-25-some-infinite-cardinality-identities/
[bh]: https://github.com/halogenica/beautifulhugo
[Google]: https://www.google.com
[mygl_macros]: /js/katex-macros.js
[mygh_macros]: https://git.io/fAjD6
[Hugo]: https://gohugo.io/
[mj_macros]: https://docs.mathjax.org/en/v3.0-latest/input/tex/macros.html#defining-tex-macros
[old_blog]: https://git.io/vtblog
[10ptb]: https://katex.org/docs/support_table.html#qr

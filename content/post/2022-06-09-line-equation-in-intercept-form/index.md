---
title: "Line Equation in Intercept Form"
subtitle: "LaTeX code for a TikZ figure"
date: 2022-06-09T09:26:54+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

This is a first draft of a TikZ picture illustraing this classical formula to be
used for math help channels.

Adding `\caption{for the picture}` without "Figure 1:" requires
`\usepackage{caption}` and wrapping with `\begin{figure}`.  It also possible to
use the primitive TeX command `\par`, but it would be complicated to use that
with `standalone`.  In the previous post, the SVG picture from the LaTeX table
in an `article` has too much useless whitespace around the table.  I don't
bother to learn other packages, as I need time for other more important stuff.

As a work around, I used `\node at (x,y) {node content}`.  Adding `[draw]`
before `{node content}` gives a rectangular box enclosing the node.

The grid lines need to be drawn before the axes.  I learnt the technique of
double `grid` lines from the classic *TikZ pour l'impatitent*.  The `[-latex]`
style comes from the LaTeX plugin in VS Code.  I used `[pos=1.05]` instead of
`[right]` and `[above]` to unify code the two axes.  The graph of the staight
line is restricted by `\clip {TikZ command for a geomteric shape}`, which is
restricted by `\begin{scope}` and `\end{scope}`.

```tex
\documentclass[tikz,border=2pt]{standalone}
\begin{document}
\begin{tikzpicture}
    \coordinate (O) at (0,0);
    \coordinate (L) at (-2.5,0);
    \coordinate (R) at (2.5,0);
    \coordinate (D) at (0,-2.5);
    \coordinate (T) at (0,2.5);
    \coordinate (A) at (1,0);
    \coordinate (B) at (0,1.5);

    \node at (0, 3.5) [draw] {
        Equation of straight line in intercept form
    };
    
    \draw[help lines, step=0.1] (-2, -2) grid (2,2);
    \draw[gray, line width=0.75, step=0.5] (-2, -2) grid (2,2);
    \draw[-latex] (L) -- (R) node[pos=1.05] {$x$};
    \draw[-latex] (D) -- (T) node[pos=1.05] {$y$};
    \begin{scope}
        \clip (-2.5,-2.5) rectangle (2.5,2.5);
        \draw[thick] plot[domain=-3:3] (\x,{-1.5*\x/1 + 1.5});
        \draw (A) node {$\times$} node[below left] {$a$};
        \draw (B) node {$\times$} node[below left] {$b$};
    \end{scope}
    \node at (0,-4) [draw, align=center] {
        $a = x\textrm{-intercept}$ \\
        $b = y\textrm{-intercept}$ \\ [1ex]
        $\displaystyle\frac{x}{a} + \frac{y}{b} = 1$
        };
\end{tikzpicture}
\end{document}
```

![straight line equation intercept form](graph.svg)
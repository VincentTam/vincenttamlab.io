---
title: "GIMP w'ont Start after Ubuntu Distro Upgrade"
date: 2020-10-09T17:04:18+02:00
categories:
- technical support
tags:
- Ubuntu
- GIMP
draft: false
---

### Problem

After upgrading to Ubuntu 20.04 form 18.04, I wasn't able to start [GIMP] 2.10.
The message was in the same format as that in a
[related Ask Ubuntu question][au1152945], except that the version numbers were
greater.

I attempted installing the package `gegl`, which was irrelavant to the problem.

### Analysis

I found the answers mentioning the package `libgegl` helpful.

```
$ dpkg -l | grep gegl
ii  gegl                                       0.4.22-3                         
     amd64        Generic Graphics Library Test Program
ii  libgegl-0.4-0:amd64                        1:0.4.18+om-0ubu18.04.18~ppa     
     amd64        Generic Graphics Library
ii  libgegl-common                             1:0.4.18+om-0ubu18.04.18~ppa     
     all          Generic Graphics Library - common files
```

Despite deletion of `ppa:otto-kesselgulasch/gimp` by `ppa-purge`, that leaves a
trace in `/etc/apt/sources.list.d`.

### Solution

A [related question on Ubuntu Francophone Forum][soln] contained the right
command to solve the problem.

```
$ sudo apt-get install --reinstall libgegl-0.4-0=0.4.22-3 \
> libgegl-common=0.4.22-3
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following packages will be DOWNGRADED:
  libgegl-0.4-0 libgegl-common
0 upgraded, 0 newly installed, 2 downgraded, 0 to remove and 0 not upgraded.
Need to get 1,519 kB of archives.
After this operation, 194 kB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://archive.ubuntu.com/ubuntu focal/universe amd64 libgegl-0.4-0 amd64 
0.4.22-3 [919 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/universe amd64 libgegl-common all 0
.4.22-3 [599 kB]
Fetched 1,519 kB in 3s (558 kB/s)       
dpkg: warning: downgrading libgegl-0.4-0:amd64 from 1:0.4.18+om-0ubu18.04.18~ppa
 to 0.4.22-3
(Reading database ... 228752 files and directories currently installed.)
Preparing to unpack .../libgegl-0.4-0_0.4.22-3_amd64.deb ...
Unpacking libgegl-0.4-0:amd64 (0.4.22-3) over (1:0.4.18+om-0ubu18.04.18~ppa) ...
dpkg: warning: downgrading libgegl-common from 1:0.4.18+om-0ubu18.04.18~ppa to 0
.4.22-3
Preparing to unpack .../libgegl-common_0.4.22-3_all.deb ...
Unpacking libgegl-common (0.4.22-3) over (1:0.4.18+om-0ubu18.04.18~ppa) ...
Setting up libgegl-common (0.4.22-3) ...
Setting up libgegl-0.4-0:amd64 (0.4.22-3) ...
Processing triggers for libc-bin (2.31-0ubuntu9.1) ...
```

[GIMP]: https://www.gimp.org
[au1152945]: https://askubuntu.com/q/1152945/259048
[soln]: https://forum.ubuntu-fr.org/viewtopic.php?id=2055578

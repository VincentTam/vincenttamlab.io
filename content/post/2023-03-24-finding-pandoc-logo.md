---
title: "Finding Pandoc Logo"
date: 2023-03-24T02:10:45+01:00
categories:
- blogging
tags:
- pandoc
- SVG
draft: false
---

### Background

I'm writing my [guide for preparing simple documents][doc_guide].  I hope that users can prepare documents efficiently and the prepared documents look smart.

### Problem

To help readers get my message in the linked guide, I used many icons from [Font Awesome][fa].  However, it doesn't include [pandoc][pandoc]'s logo.  I tried searching that using the search string "pandoc logo filetype:svg".  [Duckduckgo][ddg] couldn't find any results.  Luckily, [Google][ggl] found many SVGs.  The first result linked to a [relevant discussion on Google Groups][1st_res].  However, all posts were collapsed, and the desired SVG didn't show up.

{{< beautifulfigure
  src="../../page/simple-document-templates/pandoc-logo.svg"
  title="Pandoc logo found Google search"
  caption="see below for the link"
  alt="pandoc SVG logo" >}}

### Solution

It was tedious to find out the [message containing the desired SVG][target].  Luckily, I found it.  However, when it's diminished, the letters inside were too small to be recognised.  As a result, I've chosen another logo.

{{< beautifulfigure
  src="../../page/simple-document-templates/pandoc-logo2.svg"
  title="Another pandoc logo in the target message"
  caption="see below for the link"
  alt="pandoc SVG logo" >}}

Pandoc logos found:

- a [more complicated logo][logo1], for larger dimensions
- a [simpler logo][logo2] with only two letters, for smaller dimensions

[doc_guide]: /page/simple-document-templates/
[fa]: https://fontawesome.com/
[pandoc]: https://pandoc.org/
[ddg]: https://duckduckgo.com/
[ggl]: https://google.com/
[1st_res]: https://groups.google.com/g/pandoc-discuss/c/1bKIuyBnWaQ/discussion
[target]: https://groups.google.com/g/pandoc-discuss/c/1bKIuyBnWaQ/m/Srf5mxYvCgAJ
[logo1]: https://04037202598852537478.googlegroups.com/attach/a2f169bf9b74a/pandoc_o.svg?part=0.2&view=1&vt=ANaJVrFiQlOPmXPKyfXwyj6WSv4X9YbTD3sssvg2ephWQZzGzIwB5GSxvGoGkANuc7EdzzYPbsOEV09oER_A_agiOek0Qvj_DtOPA3a_LJbz31wV79ZODkg
[logo2]: https://04037202598852537478.googlegroups.com/attach/a2f169bf9b74a/pandoc2_2o.svg?part=0.1&view=1&vt=ANaJVrEWnQZR0z0eVmARC6pEruEsv78li83tqTevM7Z2eUmbtrFy4tGaNt8FRHlRjK6ZnghdVq-Sgt18TTJfIdtmtRgzyt12rzlFQh2h9aWZa2h4X5lGqp4

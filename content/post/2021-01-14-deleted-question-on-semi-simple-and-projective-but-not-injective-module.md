---
title: "Deleted Question on Semi-Simple and Projective but not Injective Module"
date: 2021-01-14T10:39:00+01:00
categories:
- math
tags:
- Math.SE
- backup
---

A backup of a deleted PSQ : <https://math.stackexchange.com/q/3955443/290189>

OP : [irfanmat][op]

It has a detailed answer by [Atticus Stonestrom][answerer].  It's pity that his
post got deleted.  As there's no reason for undeletion, I'm posting it here so
as to preserve the contents.

### Question body

> Is there a semi-simple and projective but not injective module? I will be glad if you help.

### Response(s)

> In the non-commutative case, the answer is yes. Consider $R$ the ring of upper
triangular $2\times 2$ matrices over a field $F$, and denote by $e_{ij}$ the
element of $R$ with the $ij$-th entry equal to $1$ and all other entries equal
to $0$. We can decompose $R$ as a direct sum of left ideals $$Re_{11}\oplus
(Re_{12}+Re_{22})=Re_{11}\oplus Re_{22},$$ so let $M=Re_{11}$. Then $M$ is
clearly simple, and – as a direct summand of the free module $R$ – is also
projective. However, $M$ is not injective; consider the map $f:Re_{11}\oplus
Re_{12}\to M$ taking $e_{11}$ and $e_{12}$ to $e_{11}$. $Re_{11}\oplus Re_{12}$
is a left ideal of $R$, but there is no way to extend $f$ to a map $R\rightarrow
M$, so this gives the desired example.
>
> ----------
>
> In the case of commutative Noetherian rings, the answer is no.
>
> **Lemma:** If $R$ is a commutative ring, and $M$ is a simple and projective $R$-module, then $M$ is injective.
>
> **Proof:** Let $m\in M\setminus\{0\}$, and consider the map $f:R\to M$ taking
$r\mapsto r\cdot m$. By simplicity, $f$ is surjective, and thus by projectivity
there is a map $g:M\to R$ such that $f\circ g=\operatorname{id}_M$. In
particular, $g$ is injective, so $I:=g(M)$ is an ideal of $R$ isomorphic to
$M$.
>
> It suffices therefore to show that $I$ is injective. Letting $J=\ker f$, we
have $R=I\oplus J$, and in particular $1=i+j$ for some $i\in I$ and $j\in J$. We
have $$i=i\cdot 1=i(i+j)=i^2+ij,$$ but $ij\in I\cap J$ and hence equals $0$,
whence $i=i^2$ is idempotent. Now, since $I$ is non-zero, $i$ is non-zero, and
thus by simplicity $I=Ri$; also, since $IJ\leqslant I\cap J=\{0\}$, $iJ=\{0\}$.
Thus $i$ acts as the identity on $I$ but sends every element of $J$ to $0$, and
so any map $h:K\rightarrow I$ from an ideal $K\leqslant R$ must send every
element of $K\cap J$ to $0$. Furthermore, by simplicity of $I$, $K\cap I$ equals
either $\{0\}$ or all of $I$. In the first case, $h$ is identically zero, so we
may extend it to the zero map from $R$ to $I$. In the second case, we can extend
$h$ to the map $\overline{h}:R\to I$ given by $\overline{h}\rvert_I = h\rvert_I$
and $\overline{h}\rvert_J=0$. Thus we can extend any map $K\to I$ to a map $R\to
I$, and so by Baer's criterion $I$ is injective. $\square$
>
> **Theorem:** If $R$ is a commutative Noetherian ring, and $M$ is a semi-simple and projective $R$-module, then $M$ is injective.
>
> **Proof:** By semisimplicity, write $M=\bigoplus_{i\in I}M_i$, where each
$M_i\leqslant M$ is simple. Since projectivity is preserved under direct
summands, each $M_i$ is projective, and hence – by our lemma – is injective.
Thus $M$ is a direct sum of injective modules, and so, because $R$ is
Noetherian, the Bass-Papp theorem gives that $M$ is injective. $\square$
>
>
> ----------
>
> I'm not sure about the commutative, non-Noetherian case. You'd need an infinite direct sum as an example, since the argument above extends to the non-Noetherian case for finite direct sums.

[op]: https://math.stackexchange.com/users/859273/irfanmat
[answerer]: https://math.stackexchange.com/users/663661/atticus-stonestrom

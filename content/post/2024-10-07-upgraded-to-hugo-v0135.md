---
title: "Upgraded to Hugo v0.135"
date: 2024-10-07T13:05:16+02:00
categories:
- blogging
tags:
- hugo
---

# Background

1. 18 months ago, I used `latest`
[Docker image in GitLab container resgitry for Hugo extended][dockerImg] at
commit [168fe3d].
1. Six years ago, I [integrated static comments to this blog][cmtIntegration].
1. Last week end, when my friend tested static comments here, she reported that
[@StaticmanLab's][staticmanLab] GitLab token had been expired.  As a result, I
updated it and re-deployed my [Staticman] instance.

# Problem

I wrote [a sample comment containing math equations][sampleCmt] to test if it's
still working.  The back-end processes were fine, but the site rebuilding wasn't
successful, as commit [c531a0b] reveals.

# Cause

Remembering that I was using the `latest` Docker image on GitLab's container
registry, I ran `hugo serve` after updating my [Hugo][gohugo] (extended verison)
to the latest version on M\$ Win\* 11.

```
$ hugo serve
Watching for changes in
C:\Users\sere\Documents\vincenttam.gitlab.io\{archetypes,content,data,layouts,static,themes}
Watching for config changes in
C:\Users\sere\Documents\vincenttam.gitlab.io\hugo.toml
Start building sites …
hugo v0.135.0-f30603c47f5205e30ef83c70419f57d7eb7175ab+extended windows/amd64
BuildDate=2024-09-27T13:17:08Z VendorInfo=gohugoio

ERROR invalid front matter: published: %!!(MISSING)s(bool=false): see
C:\Users\sere\Documents\vincenttam.gitlab.io\content\post\2022-09-05-functions-for-arts\fct-slides.md
ERROR invalid front matter: date: 18 avril 2023: see
C:\Users\sere\Documents\vincenttam.gitlab.io\content\post\2023-04-18-notes-pour-l-algo-des-tableaux\diapo.md
ERROR invalid front matter: published: %!!(MISSING)s(bool=false): see
C:\Users\sere\Documents\vincenttam.gitlab.io\content\post\2023-04-18-notes-pour-l-algo-des-tableaux\diapo.md
WARN  deprecated: .Site.IsMultiLingual was deprecated in Hugo v0.124.0 and will
be removed in a future release. Use hugo.IsMultilingual instead.
WARN  calling IsSet with unsupported type "ptr" (*page.siteWrapper) will always
return false.
WARN  calling IsSet with unsupported type "invalid" (<nil>) will always return
false.
Built in 5008 ms
Error: error building site: logged 3 error(s)
^C
[1]+  Exit 1                  hugo serve
```

When I made those HTML slides, I thought that they were part of my posts'
contents, which were placed under `contents/post/…`.  They include the Markdown
source file and the generated HTML slides.  To avoid `hugo`'s interpretation of
those Markdown source files, whose file extension name is MD, I put the
[front matter parameter `publised: false`][frontMatter] at the start of those MD
files.

However, in recent Hugo releases, HTML files are no longer included in Hugo's
output.  After some searching, I moved them to `static/` because that's the only
solution I knew.  At that time, the `published` paramter still accepted boolean
values.  The site regeneration for commit [fa2a2b4] was smooth.

```
$ git checkout fa2a2b4; git grep --name-only '^published: '; git checkout master
content/post/2022-09-05-functions-for-arts/fct-slides.md
content/post/2022-09-05-functions-for-arts/index.md
content/post/2023-04-18-notes-pour-l-algo-des-tableaux/diapo.md
```

Before [c531a0b], I didn't expect the above `ERROR` to occur.  Due to this
failure, I checked [Hugo's docs for that front matter parameter][frontMatter],
and I found that it had been changed to "alias to `publishDate`".

# Solution

The above `ERROR` output was self-explanatory.

1. Removal of that obsolete front matter parameter in `content/`.
1. Moved those Markdown source files' path from `content/` to `static/` at
commit [a199491].
1. Replaced `lastest` in `.gitlab-ci.yml` by `0.135.0` at commit [53669ec], so
that any Docker image update on GitLab container registry won't break a future
site regeneration.

# Skills reviewed

Use `:r [cmd]` to put the result of a [Vim] `[cmd]` under the cursor.

[dockerImg]: https://gitlab.com/pages/hugo/container_registry/574432
[168fe3d]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/168fe3d
[cmtIntegration]: /post/2018-09-16-staticman-powered-gitlab-pages/1/
[staticmanLab]: https://gitlab.com/staticmanlab
[Staticman]: https://staticman.net/
[sampleCmt]: /post/2023-10-08-exponential-function-series-definition/#comment-1r1
[c531a0b]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/c531a0b
[gohugo]: https://gohugo.io/
[frontMatter]: https://gohugo.io/content-management/front-matter/#published
[fa2a2b4]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/fa2a2b4
[a199491]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/a199491
[53669ec]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/53669ec
[Vim]: https://www.vim.org/

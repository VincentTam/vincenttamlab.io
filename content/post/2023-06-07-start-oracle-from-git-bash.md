---
title: "Start Oracle From Git Bash"
date: 2023-06-07T09:36:40+02:00
categories:
- technical support
tags:
- Oracle
- SQL
- Git Bash
draft: false
---

### Goal

To run Oracle SQL in Git Bash.

### Assumption

You have `sqlplus` added to your `PATH`.

```sh
export PATH=${PATH}:/c/oraclexe/app/oracle/product/11.2.0/server/bin/
```

### Problem

The option `/nolog` doesn't work as expected.

```
$ sqlplus /nolog

SQL*Plus: Release 11.2.0.2.0 Production on Mer. Juin 7 09:43:47 2023

Copyright (c) 1982, 2014, Oracle.  All rights reserved.

…

When SQL*Plus starts, and after CONNECT commands, the site profile
(e.g. $ORACLE_HOME/sqlplus/admin/glogin.sql) and the user profile
(e.g. login.sql in the working directory) are run.  The files may
contain SQL*Plus commands.

Refer to the SQL*Plus User's Guide and Reference for more information.
```

### Solution

Luckily, googling "start sqlplus from git bash", I found the solution in
[this related Oracle Forum post][soln]: using an extra slash will do, i.e.
`sqlplus //nolog`.

[soln]: https://forums.oracle.com/ords/apexds/map/tech/discussion/4265768/sql-nolog-doesnt-work-with-gitbash-on-windows

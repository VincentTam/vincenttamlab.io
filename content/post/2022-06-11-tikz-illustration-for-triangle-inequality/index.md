---
title: "Tikz Illustration for Triangle Inequality"
date: 2022-06-11T16:36:23+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

I thought the code would be easy, but it turned out that I spent one hour and
half to draw this figure.

![triangle inequality](pic.svg)

```tex
\documentclass[border=2pt,preview]{standalone}
\usepackage{tikz}
\usepackage{caption}
\newcommand{\sideA}{2}
\newcommand{\sideB}{3}
\newcommand{\sideC}{6}
\colorlet{myred}{red!40!yellow}
\colorlet{mygreen}{green!40}
\colorlet{myblue}{blue!20}
\begin{document}
\begin{figure}
\centering
\begin{tikzpicture}[thick]
\coordinate (A) at (0,0);
\coordinate (B) at (\sideC,0);
\draw[<->,>=stealth,myred] (A) -- ++ (-\sideA, 0) node [pos=0.5,below] {$\color{myred}{a}$};
\draw[<->,>=stealth,mygreen] (B) -- ++ (\sideB, 0) node [pos=0.5,below] {$\color{mygreen}{b}$};
\draw[<->,>=stealth,myblue] (A) --  (B) node [pos = 0.5, below]{$\color{myblue}{c}$};
\draw[dashed,myred] (A) circle (\sideA);
\draw[dashed,mygreen] (B) circle (\sideB);
\end{tikzpicture}
\captionsetup{labelformat=empty}
\caption{triangle inequality ${\color{myred}{a}} + {\color{mygreen}{b}} >
  {\color{myblue}{c}}$ with $\color{myred}{a = \sideA}$,
  $\color{mygreen}{b = \sideB}$, $\color{myblue}{c = \sideC}$}
\end{figure}
\end{document}
```

The colors are customized for the Discord bot TeXit.

I've forgotten how to prove this with elementary tools without using the Cosine
Law, which I provided an [interactive proof without words on Geogebra][ggb_app].

After writing this article, I learnt how to include caption into `standalone`
from [this TeX.SE answer][texse1].  The line `\captionsetup{labelformat=empty}`
removes "Figure 1: " from the caption.

[ggb_app]: https://www.geogebra.org/m/n5ucnuqk
[texse1]: https://tex.stackexchange.com/a/329275/126386

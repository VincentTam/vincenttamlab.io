---
title: "Detect Missing EOF"
date: 2018-12-23T12:10:06+01:00
type: post
categories:
- technical support
tags:
- Linux
draft: false
---

While editing files on GitHub/Gitlab, missing an empty line at the bottom of a
file in the buffer will result in no newline at the end of file.  In the case of
regular text files, this _isn't_ consistent with the POSIX standards.

```sh
for file in `git ls-files`; do
  grep -Iq . $file && if ! newline_at_eof $file; then; else echo $file; fi
done
```

Fred has provided [a shell script for detecting EOF][eof] on Stack Overflow.
I've replaced `bash` with `sh` because I'm using Zsh.

```sh
#!/bin/zsh
if [ -z "$(tail -c 1 "$1")" ]
then
    echo "Newline at end of file!"
    exit 1
else
    echo "No newline at end of file!"
    exit 0
fi
```

[eof]: https://stackoverflow.com/a/34944104/3184351

---
title: Espace de trajectoires
subtitle: Comparaison des références
date: 2018-09-28T10:41:32+02:00
type: post
categories:
- math
tags:
- probability
draft: false
---

### Tribu produit

| source | symbole | engendrée par |
| --- | --- | --- |
| Prof | $\Er{\OXT}$ | $\mathcal{C}_0 = \Big\lbrace \lbrace f \in E^\Bbb{T} \mid f(t) \in B \rbrace \bigm\vert t \in \Bbb{T}, B \in \Er \Big\rbrace$ <br> $\mathcal{C}_1 = \Big\lbrace \lbrace f \in E^\Bbb{T} \mid f(t_i) \in B_i \forall i \in \lbrace 1,\dots,n \rbrace \rbrace \newline \bigm\vert t_j \in \Bbb{T}, B_j \in \Er \forall j \in \lbrace 1,\dots,n \rbrace, n \in \N^* \Big\rbrace$ |
| Meyre | $\bigotimes_{t \in \Bbb{T}} \Er$ | des cylindres $C = \prod_{t \in \Bbb{T}} A_t$ <br> d'ensembles mesurables $A_t \in \Er$ <br> de dimension finie $\card{\lbrace t \in \Bbb{T} \mid A_t \neq E \rbrace} < \infty$ |

Je trouve $\Er{\OXT}$ plus court à écrire, tandis que
$\bigotimes_{t \in \Bbb{T}} \Er$ est plus flexible.

J'ai cassé la tête sur l'ensemble qui engendre la tribu produit pedant des
heures.  Ce n'est pas facile à voir des « cylindres mesurables de dimension
finie » dans $\mathcal{C}_0$, $\mathcal{C}_1$ non plus !

La définition dans Meyre m'a rappelé celle de la topologie produit.  Alors,
c'est plus agréable à comprendre.  A mon niveau, ce n'est pas évident à voir que

<div>
$$
\Big\{\{f \in E^\Bbb{T} \mid f(t_i) \in B_i, i \in \{1,\dots,n\}\} \bigm|
t_j \in \Bbb{T}, B_j \in \Er \forall j \in \{1,\dots,n\}\Big\}
$$
</div>

représente des cylindres sans le livre de Meyre.

### Processus

| source | Prof | Meyre |
| --- | --- | --- |
| symbole | $X: (\Omega, \mathcal{A}) \to (E^\Bbb{T}, \Er{\OXT})$ | $X: ((\Omega, \mathcal{A}), \Bbb{T}) \to E$ |
| _position aléatoire_ $X_{t_0}$ | variable _aléatoire_ <br> $X\_{t\_0}: \omega \mapsto X\_{t\_0}(\omega)$ | $X(\cdot, t_0)$ |
| une _realisation_ de <br> trajectoire dans $E^\Bbb{T}$ | function $t \mapsto X_t(\omega_0)$ | $X(\omega_0,\cdot)$ |
| remarque | plus facile à comprendre <br> au début | plus facile à comprendre <br> la tribu produit $\Er{\OXT}$ |

Comme on veut bâtir des probabilités sur des trajectoires, il faut une tribu sur
l'ensemble des trajectoires.

| objets | orthographe | symbole | explications |
| --- | --- | --- | --- |
| une instance/realisation <br> de trajectoire | miniscule | $\lbrace X_t(\omega) \rbrace_t$ | des états $X_t(\omega) \in E$ <br> indexés par $t \in \Bbb{T}$ |
| l'ensemble de trajectoires | majuscule | $E^\Bbb{T}$ | l'ensemble des fonctions <br> de $\Bbb{T}$ dans $E$ |

### Lien entre les deux références

<div>
$$f \in \left( E^\Bbb{T} \right)^\Omega \xrightarrow[\text{la définition de Meyre}]{\text{retrouve}} E^{\Bbb{T} \times \Omega}$$
</div>

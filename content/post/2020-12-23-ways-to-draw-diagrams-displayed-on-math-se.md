---
title: "Ways to Draw Diagrams Displayed on Math.SE"
date: 2020-12-23T11:46:47+01:00
categories:
- blogging
tags:
- Math.SE
- Markdown
- LaTeX
draft: false
---

I wanted to start a meta question, but I don't see a point of that after viewing
some related posts listed at the end of the next subsection.

### Intended question

You may vote on your preferred way.

| Ways | Advantages | Disadvantages |
| --- | --- | --- |
| [AMScd](https://math.meta.stackexchange.com/q/2324/290189) | supported on Math.SE for a long time | <ul><li>no diagonal arrows</li><li>syntax less well-known</li><li>Two-way arrows $\rightleftarrows$ look odd</li></ul> |
| [`array`](https://math.meta.stackexchange.com/a/2325/290189) | <ul><li>supported on Math.SE for a long time</li><li>easier syntax</li><li>write basic diagonal arrows like $\nearrow$</li></ul> | <ul><li>fine tuning spacing is hard</li><li>diagonal arrows only work for neighboring nodes</li></ul> |
| [ASCIIFlow](https://meta.stackexchange.com/a/321316/259305) | WYSIWYG interface | lines are rendered as slashes in code |
| TikZ | <ul><li>well known syntax</li><li>can draw pretty diagrams</li></ul> | not supported on SE, need to import as picture |
| others? |

Related questions:

1. https://math.meta.stackexchange.com/q/9632/290189
2. https://math.meta.stackexchange.com/q/30580/290189

### AMScd

Adapted from [MathJax tutorial for commutative diagrams](https://math.meta.stackexchange.com/a/31141/290189).

$$
\require{AMScd}
\require{enclose}
\begin{CD}
\Large{\enclose{circle}{A}} @>a>> \Large{\enclose{circle}{B}} \\
@V b V V @V 1-c \uparrow V c V \\
\Large{\enclose{circle}{C}} @>>d> \Large{\enclose{circle}{D}}
\end{CD}
$$

```
$$
\require{AMScd}
\require{enclose}
\begin{CD}
\Large{\enclose{circle}{A}} @>a>> \Large{\enclose{circle}{B}} \\
@V b V V @V 1-c \uparrow V c V \\
\Large{\enclose{circle}{C}} @>>d> \Large{\enclose{circle}{D}}
\end{CD}
$$
```

### $\rm\LaTeX$'s `array` environment

Thanks to AMScd.  The diagram below is modified from the [MathJax tutorial's answer](https://math.meta.stackexchange.com/a/31141/290189)

$$
\require{enclose}
\begin{array}{ccccccccc}
&&& \curvearrowleft\tfrac13 &\\
\Large{\enclose{circle}{A}} & \xrightarrow{0.1} & \Large{\enclose{circle}{B}} & \xrightarrow{0.2} & \Large{\enclose{circle}{C}} & \xleftarrow{0.3} & \Large{\enclose{circle}{D}} & \xleftarrow{0.4} & \Large{\enclose{circle}{E}}\\\
\scriptsize{0.5}\large{\downarrow} & \scriptsize{0.6}\large{\searrow} & \scriptsize{0.7}\large{\downarrow} & \scriptsize{0.8}\large{\nearrow} & \scriptsize{0.9}\large{\downarrow} & \scriptsize{0.1}\large{\swarrow} & \scriptsize{0.2}\large{\downarrow} & \scriptsize{0.3}\large{\nwarrow} & \scriptsize{0.4}\large{\downarrow}\\\  
\Large{\enclose{circle}{F}} & \xrightarrow[0.5]{} & \Large{\enclose{circle}{G}} & \xrightarrow[0.6]{} & \Large{\enclose{circle}{H}} & \xleftarrow[0.7]{} & \Large{\enclose{circle}{I}} & \xleftarrow[0.8]{} & \Large{\enclose{circle}{J}}\\
\circlearrowright\tfrac12\\
\end{array} 
$$

```latex
$$
\require{enclose}
\begin{array}{ccccccccc}
&&& \curvearrowleft\tfrac13 &\\
\Large{\enclose{circle}{A}} & \xrightarrow{0.1} & \Large{\enclose{circle}{B}} & \xrightarrow{0.2} & \Large{\enclose{circle}{C}} & \xleftarrow{0.3} & \Large{\enclose{circle}{D}} & \xleftarrow{0.4} & \Large{\enclose{circle}{E}}\\\
\scriptsize{0.5}\large{\downarrow} & \scriptsize{0.6}\large{\searrow} & \scriptsize{0.7}\large{\downarrow} & \scriptsize{0.8}\large{\nearrow} & \scriptsize{0.9}\large{\downarrow} & \scriptsize{0.1}\large{\swarrow} & \scriptsize{0.2}\large{\downarrow} & \scriptsize{0.3}\large{\nwarrow} & \scriptsize{0.4}\large{\downarrow}\\\  
\Large{\enclose{circle}{F}} & \xrightarrow[0.5]{} & \Large{\enclose{circle}{G}} & \xrightarrow[0.6]{} & \Large{\enclose{circle}{H}} & \xleftarrow[0.7]{} & \Large{\enclose{circle}{I}} & \xleftarrow[0.8]{} & \Large{\enclose{circle}{J}}\\
\circlearrowright\tfrac12\\
\end{array} 
$$
```

### The rest

As this post won't be published on meta, having the link to ASCIIFlow is enough.

---
title: "Copy File and Preserve Path"
subtitle: "Two bash solutions"
date: 2018-09-17T12:39:09+02:00
type: post
categories:
- technical support
tags:
- Linux
toc: true
draft: false
---

### Background

While making changes to a theme for a static site generator, I changed files
under a Git submodule included in the repo for my blog.  (e.g.
`themes/beautifulhugo`)  That's ideal for local testing, but not for version
control.  As a result, I cloned the repo for the theme to a directory _separate_
from the one for my bloge (say, `~/beautifulhugo`), and commit the changes
there, then performed a Git submodule update so as to make the workflow clean.

### Problem

To add [_proper_ support for TOC on Hugo sites][pp1], the following files in the
Git _submodule_ for my site theme have been created/edited.

- `layouts/partials/footer.html`
- `static/js/fix-toc.js`

At the _root level_ of the repo for this site, _Git views the modified submodule
as dirty_---to get things done, we have to get our hands dirty.  To clean that
up, copy the files to the separate repo (`~/beautifulhugo`) and reset the
submodule to `HEAD` will do.

    $ pwd
    ~/quickstart/themes/beautifulhugo
    $ cp layouts/partials/footer.html ~/beautifulhugo/layouts/partials
    $ cp static/js/fix-toc.js ~/beautifulhugo/static/js/fix-toc.js
    $ git reset --hard HEAD

The problem with the above `cp` command is that we have to input the names of
the directories in the relative path for _each_ affected file _twice_.  That's
_inefficient_ and succumb to typos.

### Solution

[Server Fault question&nbsp;180853][sf180853] contains two feasible solutions.

1. `cp --parents`: Mac OSX's `cp` _doesn't_ offer this option.

        $ cp --parents layouts/partials/footer.html \
        > static/js/fix-toc.js ~/beautifulhugo

2. `rsync -R`

        $ rsync -auvzR layouts/partials/footer.html \
        > static/js/fix-toc.js ~/beautifulhugo

[pp1]: /post/2018-09-17-fix-hugo-table-of-contents/
[sf180853]: https://serverfault.com/q/180853

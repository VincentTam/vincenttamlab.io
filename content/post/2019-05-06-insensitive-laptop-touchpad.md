---
title: "Insensitive Laptop Touchpad"
subtitle: "Log of my Ubuntu Tweak config"
date: 2019-05-06T15:58:43+02:00
categories:
- technical support
tags:
- Ubuntu Tweak
- Fujitsu Lifebook
- touchpad
draft: false
---

# Problem

The touchpad on my new Ubuntu 18.04 LTS laptop was _frozen_.  Toogling 🖱 with
`<Fn>-<F4>` _didn't_ worked.

# Attempt

1. Logout: _didn't_ work at all
2. Reboot: _worked_ most of the time

# Partial solution

{{< youtube GaZefnkB04o >}}

After changing the **Mouse Click Emulation** in **Keyboard & Mouse** in
[Ubuntu Tweak][ubutw] from the default "Fingers" (2 fingers for right click; 3
fingers for middle click) to "Area" (Right/middle click determined by clicked
area), one might have to wait for next reboot.

[ubutw]: https://wiki.ubuntu.com/UbuntuTweak

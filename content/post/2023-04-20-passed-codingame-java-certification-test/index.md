---
title: "Passed Codingame Java Certification Test"
subtitle: "A note to some forgotten methods"
date: 2023-04-20T10:04:14+02:00
categories:
- technical support
tags:
- Java
- Codingame
draft: false
---

### String regex replace using capture groups

Since Java SE 9, [`java.util.regex.Matcher`'s `replaceAll()`][java9replaceAll]
method supports lambda expressions.

```java
// import java.util.regex.*;
String varName = "random_string";
Pattern pattern = Pattern.compile("_([a-z])");
Matcher matcher = pattern.matcher(varName);
String result = matcher.replaceAll(m -> m.group(1).toUpperCase());
```

Source: [Arvind Kumar Avinash's answer on Stack Overflow][so67605103]

### Check characters properties using `Character`'s static methods

The `Character` class provides some static predicates like

- `isAlphabetic(char c)`
- `isDigit(char c)`
- `isLowerCase(char c)`

Source: [Java Character `isAlphabetic()` Method on Javapoint][java_point]

[java9replaceAll]: https://docs.oracle.com/javase/9/docs/api/java/util/regex/Matcher.html#replaceAll-java.util.function.Function-
[so67605103]: https://stackoverflow.com/a/67605103/3184351
[java_point]: https://www.javatpoint.com/post/java-character-isalphabetic-method

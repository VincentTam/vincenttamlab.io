---
title: "Conditional Batch File Editing"
subtitle: "Adopted Mmark for Math Posts"
date: 2019-05-01T23:56:44+02:00
categories:
- technical support
tags:
- awk
- sed
- Mmark
draft: false
toc: true
---

# Problem

During the adoption of [Mmark][1] for math posts on this blog, I had to
[insert `markup: mmark`][2] at the last line of front matter of the source file
of _each_ math post.

# Seek help

I separated this into two SO questions

1. [Sed conditional match and execute command with offset][3], and
2. [A question about AWK multiple line recognition][4].

# Solution

From #1., I learnt the use of variables in AWK scripts.  From #2, some users
explained how these variables can be used for multi-line regex search.

Here's the actual command ran.

```bash
find content/post/**/*.md -print0 |
while IFS= read -d '' -r file; do
awk '
/^- math$/ {
  if(lastLine == "categories:") {
    f=1
  }
}
{ lastLine = $0 }
/^---$/ && f { print "markup: mmark"; f=0 }
1
' $file > ${file%.*}.bak
done
```

[1]: https://gohugo.io/content-management/formats/#use-mmark
[2]: /post/2019-04-29-improved-integration-of-hugo-and-katex/#general-method
[3]: https://stackoverflow.com/q/55886116/3184351
[4]: https://stackoverflow.com/q/55887589/3184351

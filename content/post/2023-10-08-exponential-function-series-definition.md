---
title: "Exponential Function Series Definition"
subtitle: "An alternative definition of the exponential function through infinite series"
date: 2023-10-08T16:50:58+02:00
categories:
- math
tags:
- limits
draft: false
---

### Motivation

My [previous post about the definition of the exponential function][pp] has
provided *no* connection between a well-known characterization (or an
alternative definition) of the exponential function:

<div>
$$\exp(s) = \lim_{n\to\infty} \sum_{k=1}^n \frac{s^k}{k!}.$$
</div>

The term to be summed is simpler than the one in the binomial expansion of $(1 +
s / n)^n$.

### Solution

We want this sum to be as small as possible as $n \to \infty$.

<div>
$$\sum_{k=2}^n \left( 1 - \frac{n \cdot \dots \cdot (n + 1 - k)}
{\underbrace{n \cdot \dots \cdot n}_{n^k}} \right) \, \frac{x^k}{k!}$$
</div>

Observe that the fraction is a product

<div>
$$\left(1 - \frac1n\right)\left(1 - \frac2n\right) \dots
\left(1 - \frac{k-1}{n}\right).$$
</div>

The reminds me a first-order approximation inequality

<div>
$$1 - \sum_{k=1}^n x_k \le \prod_{k=1}^n (1 - x_k)$$
</div>

for each $x_k \in [0,1], k = 1,\dots,n$.

I call this "first-order approximation" because if you put $x_k = c_k
\varepsilon$ for each $k = 1,\dots,n$, the LHS would become the first-order
approximation of the product on the RHS.

This inequality is a nice induction exercise.  The base case for $n = 1$ is
trivial.  The inductive step is a bit tricky.  We can't apply the case for $n =
2$ first because we aren't sure what the range of $x_1 + \dots + x_n$ is.
Instead we apply the induction hypothesis on $1 - (x_1 + \dots + x_n)$.

<div>
\begin{align}
&\quad 1 - \sum_{k=1}^{n+1} x_k \\
&\le \left(\prod_{k=1}^n (1-x_k)\right) - x_{n+1} \\
&= \left(\prod_{k=1}^n (1-x_k)\right) (1 {\color{blue}-x_{n+1}})
\underbrace{{\color{blue}+\left(\prod_{k=1}^n (1-x_k)\right) x_{n+1}} -
x_{n+1}}_{\le 0} \\
&\le \prod_{k=1}^{n+1} (1-x_k)
\end{align}
</div>

Put $x_i = \frac{i}{n}$ for each $i = 1,\dots,k$

<div>
\begin{align}
1 - \sum_{i=1}^k \frac{i}{n} &\le \prod_{i=1}^k \left(1-\frac{i}{n} \right) <1\\
0 &< 1 - \prod_{i=1}^k \left(1-\frac{i}{n}\right) \le \sum_{i=1}^k \frac{i}{n}
= \frac{k(k+1)}{2n}
\end{align}
</div>

Put this back into the sum that we want to minimize.

<div>
\begin{align}
&\quad \sum_{k=2}^n \left( 1 - \frac{n \cdot \dots \cdot (n + 1 - k)}
{\underbrace{n \cdot \dots \cdot n}_{n^k}} \right) \, \frac{x^k}{k!} \\
&\le \sum_{k=2}^n \frac{(k-1)k}{2n} \, \frac{x^k}{k!} \\
&\le \frac{x^2}{2n} \sum_{k=2}^n \frac{{\lvert x \rvert}^{k-2}}{(k-2)!} \\
&\le \frac{x^2}{2n} \, M_{\lvert x \rvert}
\end{align}
</div>

In the last line, we used the fact that the series on the right is absolutely
convergent, which I have already shown in
[equations (4), (5) and (10) in my previous post][pp].  To end this post, take
$n \to \infty$.

[pp]: /post/2022-09-19-exponential-function-product-rule/

---
title: "Some Public APIs to Be Viewed"
date: 2023-03-16T12:29:56+01:00
categories:
- technical support
tags:
- API
- useful sites
draft: false
---

| API | description |
| :-- | :---------- |
| [JSON Placeholder][1] | mock REST APIs for development only |
| [Google Translate][2] | generate free translations up to a certain limit |
| [Open Weather Map][3] | weather prediction across the world |
| [REST Countries][4] | info about the world's countries |
| [IP API][5] | data about IP addresses |
| [Random Data API][6] | like the first one, but with a sharper focus on random data |
| [Pokemon API][7] | info about Pokemon with recent introduction of GraphQL API |

[1]: https://jsonplaceholder.typicode.com/
[2]: https://cloud.google.com/translate/docs/
[3]: https://openweathermap.org/api
[4]: https://restcountries.com/
[5]: https://ipapi.co/
[6]: https://random-data-api.com/
[7]: https://pokeapi.co/

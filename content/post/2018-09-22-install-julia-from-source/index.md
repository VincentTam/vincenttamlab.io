---
title: "Install Julia From Source"
subtitle: "Custom built Julia from GitHub"
date: 2018-09-22T21:38:43+02:00
type: post
categories:
- technical support
tags:
- Linux
- Julia
draft: false
---

### Goal

To get [Julia] installed _as a normal user_ on [RHEL&nbsp;6][pp].

### Motivation

> "[Julia] talks like [Python] but walks like [C]."

To do statistics more efficiently.

The compiled binaries often contain install scripts which put files to _shared_
folders under `/usr`.  Consequently, they have to be run as sudo privileges.
That drove me to start this lengthy Julia compilation.

### Installation

_Without sudo privileges_, I've chosen to compile [Julia] from [source][ghsrc].
I was too _lazy_ to get [the dependencies][rpmdep] fixed.  I just compiled it
_without_ [GFortran] and [pkg-config] under the `~/src` folder.

{{< beautifulfigure link="install.png" thumb="-s" alt="julia installing on RHEL 6" title="Julia v1.0.0 installing on RHEL 6" caption="Screenshot taken at around 20:00" >}}

After reassuring that the entire installation process _didn't_ require sudo
privileges, I checkout out to `v1.0.0` and `make -j 6`, hoping that _six_
threads would be run simultaneously.  However, MadScientist claimed that
[`make` _isn't_ multi-threaded][so23814510].  The CPU usage graph in the above
screenshot captured the six CPUs running at the same time.

After half an hour, the system complained that the [CMake] installed was too
_old_ (v2.8.12.2).  The process exited with a nonzero status and a message to
run `contrib/download_cmake.sh` in order to grab an update version.  After
viewing the script for `/usr`, I ran `make PREFIX=~/julia install` and waited
for an hour.

Despite some errors from the compiler and the make exit status 1, _no_ errors
were explicitly shown at the end of the process.

<i class="fas fa-exclamation-triangle fa-pull-left fa-lg" aria-hidden></i>
The compilation can take a long time.  It lasted for one and a half hour on this
workstation.  I _can't_ how long this would take if it's run on a laptop.

### Result

I added a symbolic link `~/bin/julia` pointing to `~/src/julia/julia`, which in
turn points to `~/src/julia/usr/bin/julia`.

{{< beautifulfigure src="result.png" max-width="800" alt="julia running in bash" title="Julia v1.0.0 installed on RHEL 6" caption="Screenshot taken at around 21:30" >}}

    $ julia --version
    julia version 1.0.0

[pp]: /post/2018-09-20-my-first-rhel-experience/
[ghsrc]: https://github.com/JuliaLang/julia
[rpmdep]: https://centos.pkgs.org/6/puias-computational-x86_64/julia037-0.3.7-2.sdl6.x86_64.rpm.html
[pkg-config]: https://www.freedesktop.org/wiki/Software/pkg-config/
[so23814510]: https://stackoverflow.com/q/23814510/3184351
[Julia]: https://julialang.org/
[Python]: https://www.python.org/
[C]: https://en.wikipedia.org/wiki/C_%28programming_language%29
[GFortran]: https://gcc.gnu.org/fortran/
[CMake]: https://cmake.org/

---
title: "dvisvgm Guide for TikZ Pictures with Pattern Filling"
subtitle: "A quick note of the right commands for the conversion"
date: 2022-06-21T00:31:09+02:00
categories:
- technical support
tags:
- LaTeX
- dvisvgm
- SVG
draft: false
---

### Problem

I had issues in getting a SVG from a TikZ picture containing a pattern filling:
<https://github.com/mgieseki/dvisvgm/issues/158>.  The reported issue was
created last year, and it's marked as fixed.  Nonetheless, I had this issue even
though my MikTeX was up-to-date.

### Discussion

1. I wanted to confirm whether my `dvisvgm.exe`, which had been shipped with
MikTeX, was latest.
    - I first asked at <https://github.com/mgieseki/dvisvgm/issues/184>, but I
    was referred to MikTeX's GitHub repository since it's about the
    MikTeX-packaged version of this tool.
    - I am not sure about the technical explanation from the owner of dvisvgm at
    <https://github.com/MiKTeX/miktex/issues/1112>.  It seemed that I was
    already using the latest stable version of this tool.
1. I gave a MWE illustrating the problem with the graph of *y* = (*x* + 1)² with
region under the curve filled with a pattern in the above issue.  I knew that's
probably off-topic, but the package owner might have some insights about the
problem.  It turned out that PDF → SVG was partly supported by dvisvgm.  Using
DVI → SVG, I managed to get the filling pattern, but I lost the beautiful
Computer Modern Roman fonts—that's another issue.
1. Hoping for a response from the owner for the previous problem, I started
<https://github.com/mgieseki/dvisvgm/issues/186>.  He suggested the use of
`--no-fonts` or `--font-format=woff`.  I observed no difference in the local
filesystem.  I uploaded everything I got through GitHub so that he could see
what my `dvisvgm.exe` had output.  I didn't expected that the Computer Modern
Roman fonts disappeared on GitHub side.  If I hadn't asked there, I would have
never known that it's the web browser's security measure not to load the
embedded font data in a SVG loaded from a server.

### Skills learned

Meaning of some commonly used parameters of dvisvgm:

- `--no-fonts`: all fonts rendered as path.
- `--font-format=woff`: output SVG with embedded font data in WOFF.
- `-d 2`: precise to 2 decimal places, with
- `-R`: relative positioning, to reduce output SVG file size.
- `-o`: output path with file name.
- `-b 10`: a border of 10pt.  In the manual, it's written that a list of four
numbers delimited by three commas is possible, but I've never managed that.
- `-O`: optimize for web publishing, but strips off the Computer Modern Roman
font.
- `-T`: transformation.  I've only scaled images with `S <factor>`.
- `-P`: generate SVG from PDF (instead of DVI).

### Remarks

I'm indebted to Martin Gieseking for his responses and help in the above linked
issues/questions.  His suggestions saved me hours of reading and searching, so
that I could fix some SVG files at commit [8a2122cc].

[8a2122cc]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/commit/8a2122cc

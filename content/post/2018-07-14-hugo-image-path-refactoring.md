---
title: "Hugo Image Path Refactoring"
subtitle: "Make the best of Hugo page resources"
date: 2018-07-14T14:18:28+02:00
type: post
categories:
- blogging
tags:
- Hugo
---

### Goal

To set up a reasonable content structure for my blog.

### Before Hugo

In [my old Octopress blog][1], [images][2] and [posts][3] were placed
under `source/images/posts` and `source/_posts` folders respectively.
They were so *far* apart that I needed to use copy and paste the URL's
so as to get them right.  As the size of the blog grew, I could hardly
retrieve an image without first listing the files in the post image
folder.  This significantly *reduced the efficiency* of blog writing.
In my hometown, people take efficiency seriously.

### After Hugo

At first, I followed [Jura's advice][4] and put all post images as the
site's static contents.

    ![Example image alt text](/static/path-to-image.png)

However, that *didn't* address the above problem.

Thanks to [Regis Philibert's article on Hugo page resources][5], I've
now adopted Hugo's [leaf bundle][6] structure for holding the
illustrations with each post.  An image can be added by its relative
path.  This greatly simplifies the editing process.

To avoid going deep into [Hugo's image processing][7] functionalities,
I used [ImageMagick][8] to [optimize the pictures for Web][9].

    convert image.jpg -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB image_converted.jpg

I've used `-quality 60` for largest photos in
[my post about fan cleaning][10].  Despite the reduced quality, the
display is still great.

The [Beautiful Hugo][11] has already done the config for popup images,
so the [built-in shortcode `figure`][12] will automatically create a
gallery for each page.

[1]: https://git.io/vtblog
[2]: https://github.com/VincentTam/vincenttam.github.io/tree/source/source/images/posts
[3]: https://github.com/VincentTam/vincenttam.github.io/tree/source/source/_posts
[4]: https://discourse.gohugo.io/t/solved-how-to-insert-image-in-my-post/1473
[5]: https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/
[6]: https://gohugo.io/content-management/page-bundles/
[7]: https://gohugo.io/content-management/image-processing/
[8]: https://www.imagemagick.org/
[9]: https://stackoverflow.com/a/44208640/3184351
[10]: /post/2018-07-04-fujitsu-lh532-fan-cleaning/
[11]: https://github.com/halogenica/beautifulhugo
[12]: https://gohugo.io/content-management/shortcodes/#figure

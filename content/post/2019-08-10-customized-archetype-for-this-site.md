---
title: "Customized Archetype for This Site"
date: 2019-08-10T16:28:46+02:00
categories:
- blogging
tags:
- Hugo
draft: false
---

### Background

I prefer including the date into the Permalink of a post.  Everytime I create
a new post, I type

    hugo new post/$(date -I)-post-title.md

to ensure that the date is correct.

### Problem

The `title` field in the post's front matter contained the date, which _wasn't_
something that I wanted because it's already shown next to the
<i class="fas fa-clock" aria-hidden></i> icon.

### Observations

Neither `\d` nor `[\d]` works for the PCRE character class `\d`.

### Solution

I tried piping `replaceRE`, but I _didn't_ manage to get it work.  Thanks to
[this Hugo forum answer][soln], I've finally solved the problem.

```go-html
---
title: "{{ slicestr .Name 11 | humanize | title }}"
date: {{ .Date }}
categories:
- uncategorised
tags:
- notag
draft: true
---
```

The number `11` comes from the character count of `2019-08-10-`.

[soln]: https://discourse.gohugo.io/t/piping-replace-and-slicestr/13011/6?u=vincenttam

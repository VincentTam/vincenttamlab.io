---
title: Enlarged /var Partition
subtitle: Used GParted to grow /var partition
date: 2018-10-06T08:02:36+02:00
type: post
categories:
- technical support
tags:
- Linux
- GParted
- repartition
draft: false
toc: true
embedHTML: true
resources:
- src: '181006-gparted.htm'
  title: GParted log
---

### Background

I've installed [Ubuntu 18.04 on my new laptop][pp].

### Problem

The `/var` partition was _too small_.  The system complained that only
200&nbsp;MB was left.

### Solution

1. Rebooted with my <i class="fab fa-linux" aria-hidden></i> live USB.
2. Opened GParted.
3. Moved `/tmp` partition to the left and grew it to 6&nbsp;GB.
4. Grew `/var` partition  to 16&nbsp;GB
5. Click <button type="submit"><i class="fas fa-check" aria-hidden></i></button>

<!-- more -->

![GParted screenshot](/post/2018-10-06-enlarged-var-partition/gparted-running.png)

### Results

![GParted result partition table](/post/2018-10-06-enlarged-var-partition/gparted-finished.png)

### Details [TL;DR]

Here's the GParted generated log.

{{< readHTMLFile >}}

[pp]: /post/2018-08-18-ubuntu-18-04-installation-on-fujitsu-ah557/#stage-2-try-a-live-session-before-installation

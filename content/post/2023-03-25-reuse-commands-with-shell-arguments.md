---
title: "Reuse Commands with Shell Arguments"
subtitle: "My arguments for arguments, functions and alias"
date: 2023-03-25T11:23:58+01:00
categories:
- technical support
tags:
- linux
- tcsh
draft: false
---

### Background

We often want to test the output with different values for a parameter.  Here's
an example: we have a `param`eter that
<img src="/page/simple-document-templates/pandoc-logo2.svg" alt="pandoc icon"
class="small-inline-icons"> [pandoc][pandoc] uses to <i class="fa-regular
fa-file-code fa-lg"></i> <i class="fa-solid fa-arrow-right fa-lg"></i> <i
class="fa-regular fa-file-pdf fa-lg"></i> / <i class="fa-regular fa-file-word
fa-lg"></i> compile source code files to PDF / M\$ Word / etc.

```sh
rm output.html; param=0; pandoc input$param.txt -o output.html; \
echo param = $param
```

In practice, a `param`eter can be a font, the font size, etc, and there can be
multiple parameters.

### Problem

To change the value in the parameter (e.g. `0` in `param=0`) in the above
command, one has to move the cursor to the middle---that is a tedious thing to
do.  It would be much more convenient to put the value for the `param`eter at
the end of the command, like the following line.

```sh
rm output.html; pandoc input$param.txt -o output.html; param=value
```

However, the `$param` isn't taking the `value` at the end of the above command.
Is there a way to use `param` with a `value` to be specified at the end of the
line?

### Analysis

My first idea was to invert the shell's character parsing order.  However, as an
end user of any shell (e.g. Bash, Zsh, Sh), I couldn't extract anything useful.

Then I thought about the idea of mathematical functions, which quickly
translated to <span class="fa-stack fa-xs"><i class="far fa-square
fa-stack-2x"></i><i class="fa fa-terminal fa-stack-1x fa-sm"></i></span> <i
class="fa-solid fa-scroll fa-lg"></i> shell scripts.  Unluckily, the idea of
creating another file to be made executable (by `chmod +x`) might not satisfy
users seeking one-liners.  Besides, that might not be a good idea for users
accessing remote shells because a remote user might lack the permissions to run
the command `chmod +x`.

### Solution
#### Shells supporting function calls

I have tested function calls with Git Bash.  The following works for many
well-known shells like Bash, Zsh, Sh, etc.

```sh
my_func () { rm temp.txt; touch temp.txt && echo input$1.txt; }
my_func 1
# output: input1.txt
my_func 2
# output: input2.txt
```

Here I declared a shell function `my_func`, and `$1` refers to the first
argument passed to `my_func`.

#### Shells supporting function calls

Some shells like (t)csh don't support function calls.  I tried rewriting the
above shell function with an `alias`.

```sh
alias helloWorld 'echo input$1.txt; echo do other stuff'
```

However, on tcsh, that's not working.

```sh
hellowWorld first_argument
# outputs:
# input.txt
# do other stuff first_argument
```

Thanks to [this Stack Overflow answer about tcsh alias][so20600609], I know that
`$1` is passed to the script containing the `alias` rather than the `alias`
itself.  I should use `\!:1` instead of `$1`.

```sh
alias helloWorld 'echo input\!:1.txt; echo do other stuff'
hellowWorld first_arg
# outputs:
# inputfirst_arg.txt
# do other stuff
```

[pandoc]: https://pandoc.org/
[so20600609]: https://stackoverflow.com/a/20600609/3184351

---
title: "Set up GitHub Actions for Beautiful Jekyll"
date: 2019-09-02T23:23:25+02:00
categories:
- blogging
tags:
- Jekyll
- GitHub Actions
draft: false
---

[GitHub Actions][ga] provide [CI/CD support][ci], which might interest many
[GitHub] users.  I've applied for this feature a week ago.  Luckily, my
application for the trial was approved by [GitHub].  Tonight, I've found the
motivation for making my first step in my forked repo
[VincentTam/beautiful-jekyll][vtbj].

My setup:

1. Clicked on the "**Action**" tab of the repo.
2. Chose the [workflow file template for **Jekyll**][wkfl4jkl].
3. Added a slash `/` in front of `srv` in `chmod 777 srv/jekyll`, so as to fix
the ["no such file or directory" error][err1].
4. Added the parameter `repository: {your repo name}` in the site config file
`_config.yml`.

The action should be [successfully configured][succ].

Observations:

- Setting repository name by the `env` variable `PAGES_REPO_NWO` under `name`
[_didn't_ work][err2].

[ga]: https://github.com/features/actions
[ci]: https://help.github.com/en/articles/setting-up-continuous-integration-on-github
[GitHub]: https://github.com
[vtbj]: https://github.com/VincentTam/beautiful-jekyll
[wkfl4jkl]: https://github.com/actions/starter-workflows/blob/master/ci/jekyll.yml
[err1]: https://github.com/VincentTam/beautiful-jekyll/commit/2d0370a1f93818a8a8e19dc364e8906ee76aedd7/checks#step:3:6
[succ]: https://github.com/VincentTam/beautiful-jekyll/runs/210132383
[err2]: https://github.com/VincentTam/beautiful-jekyll/runs/210128347

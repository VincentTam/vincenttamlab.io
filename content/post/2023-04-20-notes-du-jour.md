---
title: "Notes du Jour"
subtitle: "Plus importante que les techno"
date: 2023-04-20T22:26:41+02:00
categories:
- blogging
draft: false
---

Dans le classique schéma pour la gestion du projet, on met le besoin du client
d'abord.  C'est important de l'entendre avant de commencer le traf.  Sinon, on
risque de perdre des heures (ou pire des jours) sur une chose que le client ne
valide pas.  Avant d'y mettre l'effort, il est bon de lui montrer idée générale
et de lui demander un avis, afin d'éviter de lui délivrer un produit qu'il ne
veut pas.

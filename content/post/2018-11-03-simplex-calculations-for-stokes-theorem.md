---
title: Simplex Calculations for Stokes' Theorem
date: 2018-11-03T02:05:58+01:00
type: post
categories:
- math
tags:
- simplexes
draft: false
---

Oriented affine $k$-simplex $\sigma = [{\bf p}\_0,{\bf p}\_1,\dots,{\bf p}\_k]$
: A $k$-surface given by the *affine* function

  <div>
  $$
  \sigma\left(\sum_{i=1}^k a_i {\bf e}_i \right) := {\bf p}_0 +
  \sum_{i=1}^k a_i ({\bf p}_i - {\bf p}_0) \tag{1},
  $$
  </div>

  where ${\bf p}\_i \in \R^n$ for all $i \in \\{1,\dots,k\\}$.  
  In particular, $\sigma({\bf 0})={\bf p}\_0$ and for each $i\in\\{1,\dots,k\\}$,
  $\sigma({\bf e}\_i)={\bf p}\_i$.

Standard simplex $Q^k := [{\bf 0}, {\bf e}\_1, \dots, {\bf e}\_k]$
: A particular type of oriented affine $k$-simplex with the standard basis
$\\{{\bf e}\_1, \dots, {\bf e}\_k\\}$ of $\R^k$.

  <div>
  $$
  Q^k := \left\{ \sum_{i=1}^k a_i {\bf e}_i \Biggm|
  \forall i \in \{1,\dots,k\}, a_i \ge 0, \sum_{i=1}^k a_i = 1 \right\}
  $$
  </div>

  Note that an oriented affine $k$-simplex $\sigma$ has *parameter domain*
  $Q^k$.

Affine $k$-chain $\Gamma$
: a finite collection of oriented affine $k$-simplexes
$\sigma\_1,\dots,\sigma\_r$

Boundary of an oriented affine $k$-simplex $\partial \sigma$
: an affine $k-1$-chain

<div>
$$
\partial \sigma = \sum_{j=0}^k (-1)^j \;\underbrace{
[{\bf p}_0,\dots,{\bf p}_{j-1},{\bf p}_{j+1},\dots,{\bf p}_k]}_{{\bf p}_j
\text{ removed}} \tag{2}
$$
</div>

We shall make _no_ use of the following proposition.

> **Proposition** Let $\sigma$ be an oriented affine $k$-simplex.
> Then $\partial^2 \sigma = 0$.
>
> _Proof_: In this $k-2$-chain, each $k-2$-simplex with ${\bf p}\_i$ and
> ${\bf p}\_j$ removed can be obtained in two ways.  WLOG, assume $i < j$.
>
> 1. ${\bf p}\_i$ removed first, followed by ${\bf p}\_j$.  These two operations
> give factors $(-1)^i$ and $(-1)^{j-1}$.  The "$-1$" in the exponent "$j-1$" is
> a result of ${\bf p}_j$'s left-shifting after removal of ${\bf p}_i$.
> 2. ${\bf p}\_j$ removed first, followed by ${\bf p}\_i$.  These two operations
> give factors $(-1)^j$ and $(-1)^i$.  Removing ${\bf p}\_j$ *doesn't* affect
> ${\bf p}\_i$'s position.
>
> These two factors cancel each other.
>
> $$
> \tag*{$\square$}
> $$

Integral of a $0$-form over an _oriented 0-simplex_
: Let $\sigma = \pm {\bf p}\_0$ be an _oriented 0-simplex_.
$\int\_\sigma f := \pm f({\bf p}\_0)$

This definition seems strange to me since the integral over a singleton in $\R$
of any real valued integrable function $f$ is zero.  However, I have to accept
this to make a progress.

In the proof of Stokes' Theorem, the author has taken $\sigma = Q^k$.

<div>
$$
\begin{aligned}
\partial \sigma &= [{\bf e}_1,\dots,{\bf e}_k]+\sum_{i=1}^k (-1)^i \tau_i \\
&= (-1)^{r-1} \tau_0 + \sum_{i=1}^k (-1)^i \tau_i,
\end{aligned}
$$
</div>

where

<div>
$$
\tau_0 =
[{\bf e}_r, {\bf e}_1, \dots, {\bf e}_{r-1}, {\bf e}_{r+1},\dots, {\bf e}_k],
$$
</div>

and

<div>
$$
\tau_i =
[{\bf 0}, {\bf e}_1, \dots, {\bf e}_{i-1}, {\bf e}_{i+1},\dots, {\bf e}_k]
$$
</div>

for $i \in \\{1,\dots,k\\}$.
Each $\tau\_i$ admits $Q^k$ as its parameter domain.

Put ${\bf x} = \tau\_0({\bf u})$ with ${\bf u} \in Q^{k-1}$.  I need some
straightforward calculations to know each coordinate of $\bf x$.

<div>
$$
\begin{aligned}
{\bf x} &= \tau_0({\bf u}) \\
&= [{\bf e}_r, {\bf e}_1, \dots, {\bf e}_{r-1}, {\bf e}_{r+1}, \dots,
{\bf e}_k]({\bf u}) \\
&= {\bf e}_r + u_1 ({\bf e}_1 - {\bf e}_r) + u_2 ({\bf e}_2 - {\bf e}_r) +
\dots + u_{r-1} ({\bf e}_{r-1} - {\bf e}_r) \\
&+ u_r ({\bf e}_{r+1} - {\bf e}_r) + \dots + u_{k-1} ({\bf e}_k - {\bf e}_r)\\
&= \sum_{j=1}^{r-1} u_j {\bf e}_j + \left(1 - \sum_{i = 1}^{k-1} u_i \right)
{\bf e}_r + \sum_{j=r+1}^k u_{j-1} {\bf e}_j \\[4ex]
\therefore x_j &= \begin{cases}
u_j & 1 \le j \le r-1 \\
1 - \sum_{i = 1}^{k-1} u_i & j = i \\
u_{j-1} & r+1 \le j \le k
\end{cases}
\end{aligned}
$$
</div>

Reference: Rudin's PMA

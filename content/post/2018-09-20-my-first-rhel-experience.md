---
title: "My First RHEL Experience"
subtitle: "Package installation as a normal user"
date: 2018-09-20T21:47:02+02:00
type: post
categories:
- technical support
tags:
- Linux
- RHEL
draft: false
---

### Introduction

This article records my errors and difficulties encountered on the first day I
came across Red Hat Enterprise Linux&nbsp;7 in my school's laboratory, as a
_normal user without sudo privileges_.

The login screen was gdm, and the desktop environment was GNOME.  IBus was used
as the input engine.

### Packages installed

The principal goal is to install tools that I usually use on RHEL _without_ sudo
permissions.  To do so, I've downloaded the executable binaries or source code
of these packages.  As I wanted to focus on my studies, I prefer downloading
executable binaries.

| Package | installation method |
| --- | --- |
| [Go] | installed from binary archive |
| [Hugo] | compiled from source code |
| [Zsh] | compiled from source code |
| [Oh My Zsh][omz] | ran installation script downloaded from GitHub |
| [Sublime Text 3][st3] | installed from binary archive |

### Go

I cloned the <i class="fab fa-git" aria-hidden></i> repo before a _failed_
compilation.  After knowing that [Go] can be installed by extracting a gzipped
tarball, I grabbed one marked Darwin.  The decompression took several minutes.
After that, bash _couldn't_ execute the binary file `go`.  It took me fifteen
minutes to realize that Darwin is the name for an OS X release.

Then, I downloaded the right archive, issued `go version` and got "permission
denied".  The files in `./bin` were _not_ marked as executable.  After fixing
this, the command finally gave "1.11".

The first linked article suggested setting `GOROOT` to `/usr/local/go`. However,
as `/usr` contains common packages for normal users, dropping a package there
requires write access, and the directory is owned by root.  Therefore, I've
chosen `~/bin/go` instead.  The `go` _binary file_ is located at `bin/go`
relative to `GOROOT`, so that expands to `~/bin/go/bin/go`.

The official Go installation verification is slightly better than using `go
version`.  A simple Hello World reminded me to mark `./pkg` as executable.
After that, the program succeeded in greeting the world.

I found `$HOME/go/Proj/Proj1` a bit strange and too long as `GOPATH`.  The
official guide suggested `$HOME/go` _without_ requiring us to put `GOPATH` into
`PATH`.  Nevertheless, I included it back later during [Hugo] installation.

References:

1. https://tecadmin.net/install-go-lang-on-centos/
2. https://golang.org/doc/install?download=go1.10.3.linux-amd64.tar.gz#install

### GitLab SSH key

SSH connection to the Git service provider is preferred over HTTPS connection.
SSH allows pushing commits _without_ typing the user password every time, once
the private key has been added to the SSH agent.

Reviewing GitLab's official guide, I've learnt that flag `-o` in

  $ ssh-keygen -o -t rsa -C "user@example.com" -b 4096

provides additional security to the private key by raising the difficulty of a
brute-force attack.  Under this flag, the newly generated keypairs _aren't_
compatible with the PEM format.

After

1. `~/.ssh/config`
2. evaluating the SSH agent's PIDstow
3. adding the SSH private key to the SSH agent

I tried cloning the [GitLab repo for this blog][repo], but the SSH agent refused
that operation, despite my _matching_ keypairs.  I double checked the public key
in my GitLab account to ensure that it matched exactly with my local copy.

I searched using phrases from the error message for an hour.  An environmental
variable catched my eyes: `SSH_AUTH_SOCK`.  Some post suggested setting this to
zero.  In my RHEL session, this was set to `/run/user/[some-number]/keyring/ssh`.
It said somewhere that one needed to disconnect from the current network and
reconnect to it so that the SSH configurations can take effect.  To my surprise,
after clicking on the "off" button, the _system hanged_.  I _couldn't_ switch to
TTY. Since the room accommodates over twenty work stations, and there were over
ten rooms, I thought that they were virtualized machines: the work station got
their power from the LAN.  Once it's cut, it lost its power.  I resorted to
press the power off button---that's bad for HDD hard disk.

I wasted time on finding a mistaken step.  In fact, I had done _nothing_ wrong.
It remained to reboot the machine.  I finally managed to use Git over SSH.

### Japanese input

The Japanese input was following a French keyboard layout, and there's _no_ way
to change it.  Click on the keyboard button _didn't_ display a popup window with
a keyboard layout, unlike other input methods.

### Unsuccessful GNU Stow installation

The symlink generator [GNU Stow][stow] can be utilized to [handle dotfiles][pp1].
The installation dependency includes some test modules of [Perl].  Omitting them
and `make && make install` after `./configure` can get it compiled, but the lack
some modules prevents it from running.

I _don't_ wish to spend time on resolving this dependency problem because

1. I lack knowledge in [Perl];
2. I lack time;
3. there're _other_ ways to handle dotfiles _without_ [GNU Stow][stow].

Nonetheless, I've learnt to add `--prefix=/custom/path` after `./configure` to
specify the installation path for `make install`.

### Zsh

I would like to have a compiled binary archive, but I could only find the source
code.  Luckily, the compilation was successful, and [Zsh] was installed in
`~/bin/zsh/`.

### Oh My Zsh

I copied the command for installing the script, which attempted to run `chsh`.
This failed since it's _impossible_ to register my own compiled shell into
`/etc/shells`.  I was told to change the default shell to [Zsh], which I would
love to.  I tried `export SHELL=$(which zsh)` and `exec -l $(which zsh)` in
either `~/.bashrc` and `~/.bash_profile`.  However, gdm's login screen
_automatically logged me out_ after a successful login.  I had to use TTY to
undo these changes.

Since then, to use [Zsh] (with [Oh My Zsh][omz]), I need to key in `zsh` under
bash.

### Sublime Text 3

Among [the softwares in the above table](#packages-installed),
the installation of [Sublime Text&nbsp;3][st3] was the _easiest_:

1. One click for downloading the compressed binary tarball.
2. Another click to opening it in a GUI archive management tool.
3. Third click to choose a target path for the decompression.  (I've chosen
`~/bin/`.)
4. (Optional) Add the installed Sublime Text&nbsp;3 directory to `PATH`.

[Go]: https://golang.org/
[Hugo]: https://gohugo.io/
[Zsh]: https://sourceforge.net/projects/zsh/
[omz]: https://github.com/robbyrussell/oh-my-zsh
[st3]: https://www.sublimetext.com/3
[repo]: https://gitlab.com/VincentTam/vincenttam.gitlab.io
[stow]: https://www.gnu.org/software/stow/
[pp1]: /post/2018-07-08-stow-your-dotfiles/
[Perl]: https://www.perl.org/

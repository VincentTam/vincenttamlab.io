---
title: "Basic Symmetries in Nine-Point Circle"
subtitle: "Personal reading report"
date: 2022-06-04T22:29:26+02:00
categories:
- math
tags:
- nine-point circle
draft: false
---

### Motivation

Someone on Discord asked about the existence of the nine-point circle.  It's
well-known that that can be proved by homothety.

### Little reminder about homothety

Homothety preserves angles (and thus parallel lines).  Homothetic polygons are
similar, so the ratio of the corresponding sides is the same.  Considering the
radii of a circle under a homothety, we see that a homothety maps a circle to
another circle.

### Notation

- *H*: orthocenter
- *G*: centroid
- *O*: circumcenter
- *ω*: circumcircle
- *H<sub>A</sub>*: feet of altitude with respect to *A*.
- *M<sub>A</sub>*: midpoint of side *a*.
- *E<sub>A</sub>*: Euler point with respect to *A*. (i.e. midpoint of *A* and
*H*)

### Problem

The [second proof for nine-point circle on AoPS][aops_proof] starts with a
proved fact that the reflection of *H* about *a* and *M<sub>A</sub>* lie on *ω*.

The first part is easy.  I changed the definition of *H<sub>A</sub>*' from
"reflect(*H*, *a*)" to "*AH* ∩ *ω*".

![nine point circle symmetry](aops1.png)

In either definition, we can prove △*BHC* ≅ △*BH<sub>A</sub>C*.  In my
definition, use ∠*CBH<sub>A</sub>*' = ∠*CAH* = ∠*CBH* = 90° − *C*.

I found the second part is harder because I used *A*' := *HM<sub>A</sub>* ∩ *ω*"
instead of "reflect(*H*, *M<sub>A</sub>*)".  After thinking about that for
hours, I gave up and searched for "nine point circle homothety" for answer, but
the information was too advanced for me.  Finally, I searched with more relevant
keywords "orthocenter reflection", and I found the hints from
[this relevant Math.SE question][mathse].

### Solution 1

In the linked solution on Math.SE, *A*' := reflect(*H*, *M<sub>A</sub>*).  It's
easy to see that *BA*'*CH* is a parallelogram, so ∠*BA*'*C* = ∠*BHC* = *B* + *C*
= 180° − *A*.  As a result, *A*' ∈ *ω*.

### Solution 2

Using my definition of *A*', just reverse the reasoning above.

We see that *A*'*C* ⟂ *AC* and *A*, *A*', *C* ∈ *ω*, so *AA*' is a diameter for
*ω*.  This shows that *AH* = 2*OM<sub>A</sub>*.

### Solution 3

On [this post on AoPS][aops_post], *A*' := reflect(*A*, *O*).

### Corollary

The nine-point circle is the image of the circumcircle *ω* under the homothety
h(*H*, 1/2).

[aops_proof]: https://artofproblemsolving.com/wiki/index.php/Nine-point_circle
[mathse]: https://math.stackexchange.com/q/1428791/290189
[aops_post]: https://artofproblemsolving.com/community/c2231h1039055_nine_points_circle_theorem
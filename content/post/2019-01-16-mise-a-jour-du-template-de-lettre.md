---
title: "Mise à jour du template de lettre"
subtitle: "Template de lettre en Xe$\\LaTeX$"
date: 2019-01-16T15:04:54+01:00
type: post
categories:
- technical support
tags:
- LaTeX
- babel
- français
draft: false
---

### Contexte

J'ai publié [mon template de lettre][1] en Xe$\LaTeX$ sur [mon ancien blog][2]
(hébergé sur GitHub Pages) il y a trois ans.

### Problème et solution

Après le [changement de l'ordi portable][3], il me faut l'installation du
pacquetage "texlive-lang-french" sur la nouvelle version d'Ubuntu.  Sinon, une
erreur du pacquetage Babel se surviendra.

    ! Package babel Error: Unknown option `frenchb'. Either you misspelled it
    (babel)                or the language definition file frenchb.ldf was not foun
    d.

    See the babel package documentation for explanation.
    Type  H <return>  for immediate help.


    l.445 \ProcessOptions*

    ?

A ce stade, Xe$\LaTeX$ parviendra à compiler le fichier $\TeX$ avec une remarque
à propos de l'obsolescence de l'option `frenchb` pour le pacquetage Babel.

    Package babel-french Warning: Option `frenchb' for Babel is *deprecated*,
    (babel-french)                it might be removed sooner or later. Please
    (babel-french)                use `french' instead; reported on input line 35.

    (/usr/share/texlive/texmf-dist/tex/generic/babel-french/french.ldf
    (/usr/share/texlive/texmf-dist/tex/generic/babel/babel.def
    (/usr/share/texlive/texmf-dist/tex/generic/babel/xebabel.def
    (/usr/share/texlive/texmf-dist/tex/generic/babel/txtbabel.def))))))
    (/usr/share/texlive/texmf-dist/tex/latex/carlisle/scalefnt.sty) (./rna.aux)
    (./rna.tns)
    No file rna.odt.

C'est clair qu'il est mieux de remplacer `frenchb` par `french` pour une
meilleure compilation.

[1]: https://vincenttam.github.io/blog/2016/04/16/latex-french-letter-template/
[2]: https://git.io/vtblog
[3]: /post/2018-08-18-ubuntu-18-04-installation-on-fujitsu-ah557/

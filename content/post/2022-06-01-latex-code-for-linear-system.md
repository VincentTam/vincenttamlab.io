---
title: "LaTeX Code for Linear System"
date: 2022-06-01T16:25:10+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

<div>
\begin{align}
A\mathbf{x} &= \mathbf{b} \\
\begin{pmatrix}
a_{11} & a_{12} & a_{13} \\
a_{21} & a_{22} & a_{23} \\
a_{31} & a_{32} & a_{33}
\end{pmatrix}
\begin{pmatrix} x_1 \\ x_2 \\ x_3 \end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix} \\
\begin{bmatrix}
\vert & \vert & \vert \\
\mathbf{a}_1 & \mathbf{a}_2 & \mathbf{a}_3 \\
\vert & \vert & \vert
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix}
&= \mathbf{b} \\
x_1 \mathbf{a}_1 + x_2 \mathbf{a}_2 + x_3 \mathbf{a}_3 &= \mathbf{b} \tag{$\star$} \\
\begin{pmatrix}
a_{11} x_1 + a_{12} x_2 + a_{13} x_3 \\
a_{21} x_1 + a_{22} x_2 + a_{23} x_3 \\
a_{31} x_1 + a_{32} x_2 + a_{33} x_3
\end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix} \\
x_1 \begin{pmatrix} a_{11} \\ a_{21} \\ a_{31} \end{pmatrix} +
x_2 \begin{pmatrix} a_{12} \\ a_{22} \\ a_{32} \end{pmatrix} +
x_3 \begin{pmatrix} a_{13} \\ a_{32} \\ a_{33} \end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix}
\end{align}
</div>

Source code:

```tex
\begin{align}
A\mathbf{x} &= \mathbf{b} \\
\begin{pmatrix}
a_{11} & a_{12} & a_{13} \\
a_{21} & a_{22} & a_{23} \\
a_{31} & a_{32} & a_{33}
\end{pmatrix}
\begin{pmatrix} x_1 \\ x_2 \\ x_3 \end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix} \\
\begin{bmatrix}
\vert & \vert & \vert \\
\mathbf{a}_1 & \mathbf{a}_2 & \mathbf{a}_3 \\
\vert & \vert & \vert
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix}
&= \mathbf{b} \\
x_1 \mathbf{a}_1 + x_2 \mathbf{a}_2 + x_3 \mathbf{a}_3 &= \mathbf{b} \tag{$\star$} \\
\begin{pmatrix}
a_{11} x_1 + a_{12} x_2 + a_{13} x_3 \\
a_{21} x_1 + a_{22} x_2 + a_{23} x_3 \\
a_{31} x_1 + a_{32} x_2 + a_{33} x_3
\end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix} \\
x_1 \begin{pmatrix} a_{11} \\ a_{21} \\ a_{31} \end{pmatrix} +
x_2 \begin{pmatrix} a_{12} \\ a_{22} \\ a_{32} \end{pmatrix} +
x_3 \begin{pmatrix} a_{13} \\ a_{32} \\ a_{33} \end{pmatrix}
&=
\begin{pmatrix} b_1 \\ b_2 \\ b_3 \end{pmatrix}
\end{align}
```

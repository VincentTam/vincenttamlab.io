---
title: "Merge Two PDF to Single Encrypted PDF"
date: 2023-03-15T17:47:14+01:00
categories:
- technical support
tags:
- PDF
- QPDF
draft: false
---

### Problem

I have unprotected

1. `input1.pdf`
1. `input2.pdf`

and I want to create one single `encrypted.pdf`.

### My try

I looked up [QPDF's manual][qpdf_man] and tried the following command.

```
qpdf --empty --pages input{1,2}.pdf --encrypt upw opw 256 -- encrypted.pdf
```

but I got this error.

```
qpdf: unrecognized argument --encrypt (pages options must be terminated with --)

For help:
  qpdf --help=usage       usage information
  qpdf --help=topic       help on a topic
  qpdf --help=--option    help on an option
  qpdf --help             general help and a topic list
```

### Solution

The sentence inside the parentheses says it all.

```
qpdf --empty --pages input{1,2}.pdf -- --encrypt upw opw 256 -- encrypted.pdf
```

[qpdf_man]: https://qpdf.readthedocs.io/en/stable/cli.html#page-selection
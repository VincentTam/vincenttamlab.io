---
title: "Staticman Script in Pure JS"
subtitle: "Staticman without jQuery"
date: 2019-08-05T09:56:16+02:00
categories:
- blogging
tags:
- JavaScript
- Staticman
draft: false
---

[The official Staticman script sample][script] depends on [jQuery].  I have been
using [jQuery] in my Staticman script for [nested comments][pp] for more than
six months.

Recently, in the [new release of Hugo Swift Theme][hstv1], in which I'm a
collaborator, one has removed all [jQuery] methods at commit [`f137efa4`][cmt].
Here's the structure of the script:

1. auxiliary functions like `elem`, `elems`, `pushClass`, `deleteClass`, etc.
2. A self-executing function `comments()`
    - some necessary local variables
    - `handleForm()` consisting merely of an event listener handling form
submission.
        + `formToJSON(form)` for converting the comment form to a JSON string.
        + `fetch(url, {...}).then(...).catch(...)` for submitting a sever
        request and handling its response.  It calls `showModal(...)` and it
        contains `resetForm()`.
    - a conditional `form ? handleForm(form) : false;` to execute the above
    method provided the existence of the comment form.
    - `closeModal()` and `showModal()` for closing and showing the modal
    respectively.
    - a conditional `containsClass(body, show) ? closeModal() : false;`.
    - a self-executing `toogleForm()` function.  Nesting a self-executing
    function inside another one [limits the namespace of variables][so].  Even
    though there's no new variable defined inside this function, there's a need
    to wrap it with parenthesis `()` because `toggleForm()` is _not_ called
    elsewhere, unlike `toggleMenu()`.  If the `if(button){...}` statement comes
    out directly, we will _not_ know that it's doing.

[script]: https://github.com/eduardoboucas/popcorn/blob/gh-pages/js/main.js
[jQuery]: https://jquery.com/
[pp]: https://vincenttam.gitlab.io/post/2019-07-30-more-static-nested-comments/
[hstv1]: https://github.com/onweru/hugo-swift-theme/releases/tag/v1.0.0
[cmt]: https://github.com/onweru/hugo-swift-theme/commit/f137efa4cc8cd37d146553666dc007ee11a2cbba#diff-fd41383df4389ed6ee75f68becb796d1
[so]: https://stackoverflow.com/a/592414/3184351

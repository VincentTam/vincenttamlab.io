---
title: "Functions for Arts"
subtitle: "Summary of my two-day work"
date: 2022-09-05T20:38:06+02:00
categories:
- math
tags:
- functions
- pandoc
- CSS
draft: false
---

1. [my HTML slides][slides]
2. You may view the source code of this blog to see the source files.

### Goal

To provide an introduction to the formal definition of functions for arts
stream students.

I aim to

1. minimize the calculations
2. use daily-life examples
3. favor pictures over text.

### IT skills used
#### Inkscape

1. `C-S-a` for alignment.
1. `C-S-f` for boundary and colors, and line patterns (i.e. arrows).
1. `C-S-d` for document size.
1. clipping process:
    1. prepare the object to be clipped.
    1. prepare the clip, which is on top of the previous object.
    1. select **Object** → **Clip** and then the first option from the top menu.

#### dvisvgm

Use the TEX → DVI → SVG route with `latex` and this tool with options
`--font-format=woff` and `-b 5 -R -d 2` to save file size and to ensure
that the SVG can be correctly opened on Inkscape.

#### pandoc skills
##### create HTML slides

The item 16 of [pandoc demo][demo] provides a great template to start with.
It is LaTeX code.  I changed the math rendering engine to [KaTeX].

    pandoc -s --katex -i -t slidy fct-slides.md -o fct-slides.html

##### custom CSS

The option `-c foobar.css` allows custom CSS.  This is handy for styling a group
of objects, like

| CSS class name | meaning          |
| :------------- | :--------------- |
| `dom`          | domain           |
| `codom`        | codomain         |
| `rng`          | range            |
| `tbdefN`       | definition table |

Although pandoc doesn't support CSS class markup like `{:myclass}`, but the
owner gave a way to add custom CSS classes to Markdown code for
images/tables/etc: wrap it with `<div class="myclass">`.

##### tables

I relied on the [pandoc User manual][manual]'s sample Markdown tables.

It's a good idea to use more lengthy markers and spaces for Unicode characters
that are greater than one byte.

Simple tables with headers

```
  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table: table caption.
```

Simple tables without headers

```
-------     ------ ----------   -------
     12     12        12             12
    123     123       123           123
      1     1          1              1
-------     ------ ----------   -------
```

The bottom line is necessary.

Multiline tables

```
-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
-------------------------------------------------------------

Table: table caption.
```

I used this for showing tables with pictures.  To include multiline contents, I
used `<br>`.

Simple grid tables with caption

```
: table caption

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+
```

The advantage of grid tables is that it allows lists and nested tables.

I used this for rendering three Sudoku tables in three columns.  I failed to
created nested simple tables. 

The header line can be changed to

```
+==============:+:==============+:==================:+
```

to allow alignment.

The `=` can be replaced with `-` to get rid of the table header.

The usual pipe tables are useful when dealing with cell contents whose code is
of variable length.

##### suppress picture caption

[This Stack Overflow question][so45030895] provides an easy way to take away the
picture caption generated from the `alt` attribute.

    ![This is the caption](/url/of/image.png) \


#### Ti*k*Z

I lacked the time to revise the use of the intercept library.  As a workaround,
I used a circular clip whose radius is larger than the object by `0.01` so that
the boundary was shown.

```tex
\begin{scope}
  \clip (3.2,-2) circle (0.31);
  \clip (3.6,-2) circle (0.31);
  \draw[color=red,pattern color=red,pattern=horizontal lines] (3.2,-2) circle (0.3);
  \draw[color=blue,pattern color=blue,pattern=vertical lines] (3.6,-2) circle (0.3);
\end{scope}
\node at (3.5,-2) [right] {$A \cap B$};
```

![venn diagram](venn2.svg)

Since objects drawn later appeared on top, for illustrating set differences, I
first drew the larger circle, then I covered it with a white circle with a
colored boundary.

```tex
\begin{scope}
  \clip (2.3,-2.7) circle (0.31);
	\draw[color=blue,pattern color=blue,pattern=vertical lines] (2.3,-2.7) circle (0.3);
	\draw[color=blue,fill=white] (1.9,-2.7) circle (0.3);	
\end{scope}
\node at (2.3,-2.7) [right=0.3cm] {$B \setminus A$};
```

#### CSS

I've put aside CSS for a long time.  Each time I tried to use it, my mind would
be bombarded by unknown errors.  VS Code sped up the process a bit thanks to its
code completion.  It's lucky that I didn't have to resort to `! important` this
time.

##### Comments

Unlike JavaScript, `//` doesn't work.

    /* Comments should look like this. */

##### image float right

I didn't have the time to explore the difference between `float` and `clear`.

```css
div.floatR img {
  float: right;
  width: 30%;
  height: 30%;
}
```

In the slide *Set operations*, I floated the above SVG to the top right hand
corner, but it occupied one line on the left.  I failed to find out why.  Since
this is free teaching, I let it go.

##### Decoration for keyboard keys

The HTML tag `<kbd>` represents a keyboard key.  I copied from
[another blog][blog].  I lack the energy to learn the `display` and `padding`.

```css
kbd {
  border: 2px solid #000000; 
  box-shadow: 2px 2px #000000; 
  font-size: .85em; 
  line-height: .85em; 
  display: inline-block;
  font-weight: 600;
  letter-spacing: .05em; 
  padding: 3px 5px;
  white-space: nowrap;
}
```

##### Table grid lines

- `border-collapse: collapse;` without this line, there'll be double grid lines.
- [`table-layout: fixed;`][tblayout] first row cells determine columns' width.
This speeds up the rendering.  `auto` is the default.
- `border-left` and `border-top` work with the `nth-child` below to give black
solid grid lines.
- `font-family: monospace;` is used to get more uniform output.

```css
div.sudoku table {
  border-collapse: collapse;
  table-layout: fixed;
  border-left: 1.5px solid #000000;
  border-top: 1.5px solid #000000;
  font-size: 0.6em;
  font-family: monospace;
  text-align: center;
  line-height: 1em;
}
```

##### Alternate table grid lines

In the final slide for *Implication (⟹)*, I included three Sudoku grids.  The
3×3 grids are in black solid lines, while the rest are in grey dotted lines.

```css
div.sudoku table tr:nth-child(3n) {
  border-bottom: 1.5px solid #000000;
}
div.sudoku table td:nth-child(3n) {
  border-right: 1.5px solid #000000;
}
div.sudoku table tr:nth-child(3n-1), div.sudoku table tr:nth-child(3n-2) {
  border-bottom: 0.7px dashed #888888;
}
div.sudoku table td:nth-child(3n-1), div.sudoku table td:nth-child(3n-2) {
  border-right: 0.7px dashed #888888;
}
```

##### Responsive YouTube Embed

The [YouTube]'s "share" → "embed" button generates an `<iframe>` with fixed
dimensions.  With some online search, I found this
[YouTube embed code generator][ytembed] incredibly convenient to use.  It
accepts the link of the YouTube video as input, and it outputs a `<div>`
container with the `<iframe>` inside, followed by a `<style>` tag.

```css
.embed-container {
  position: relative;
  padding-bottom: 56.25%;
  height: 0;
  overflow: hidden;
  max-width: 100%;
}
.embed-container iframe, .embed-container object, .embed-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 80%;
  height: 80%;
}
```

I changed the `100%` to `80%` in the `width` and `height` since my last slide
*Comments and critiques* contained a header and a line of text.  They took up
some vertical space.

```html
<div class='embed-container'>
  <iframe src='https://www.youtube.com/embed//4WNKU37aWrE?start=816'
frameborder='0' allowfullscreen></iframe>
</div>
```

In the generated code, there's no `?start=816`.  I appended this to the embed
link, so that the video would start from the specified time when the play button
is clicked.

#### Hugo

The source of my HTML slides is in Markdown.  To stop [Hugo] from rendering it,
use the following header.

```
---
published: false
---
```

The file would be ignored by Hugo, but not [Git].  Therefore, the source code
would be stored on the remote Git repo.

[slides]: fct-slides.html
[source]: fct-slides.md
[demo]: https://pandoc.org/demos.html
[manual]: https://pandoc.org/MANUAL.html#extension-multiline_tables
[KaTeX]: https://katex.org/
[so45030895]: https://stackoverflow.com/q/45030895/3184351
[Hugo]: https://gohugo.io/
[Git]: https://git-scm.com/
[blog]: https://chrispennington.blog/blog/add-styling-to-keyboard-keys-css/
[tblayout]: https://developer.mozilla.org/en-US/docs/Web/CSS/table-layout
[YouTube]: https://www.youtube.com/
[ytembed]: https://embedresponsively.com/

---
title: "Rodéo"
date: 2018-07-17T11:40:17+02:00
type: post
categories:
- français
tags:
- rodéo
---

- au sens propre: (comme en anglais)

    Un événement dans lequel un cavalier essaie de demeurer sur un cheval
avec deux jambes levées.

    {{< beautifulfigure src="rodeo-cheval.png" alt="Texture, Silhouette, Rodeo, Horse, Jumper, Mane" title="Rodéo au sens propre" caption="Source: Pixabay" >}}

- au sens figuré dans l'actualité

    Une façon dangereuse de conduite dont le conducteur lève la(les)
    roule(s) antérieure(s).

    {{< beautifulfigure src="rodeo-moto.png" alt="Motorcycle, Rear, Road, Rider, Moto Motorcycle" title="Rodéo au sens figuré" caption="Source: Pixabay" >}}

Le dicto Collins a recordé [une utilisation très rare][1] de ce mot.
Récemment, on l'utilise plus fréquemment en raison de la lutte contre
les rodéos sauvages.

[1]: https://www.collinsdictionary.com/dictionary/french-english/rod%C3%A9o

---
title: "Comparaison of Two Docker Images for Hugo"
date: 2018-12-21T21:56:57+01:00
type: post
categories:
- blogging
tags:
- Hugo
- GitLab
- Docker
draft: false
---

Having importing the repo for the [Hugo] theme [Introduction] from [GitHub] to
[GitLab], I added the automatically generated GitLab CI config file and I ran
[job #135854407][err].

    $ cd exampleSite
    $ hugo --themesDir=../.. -d ../public
    hugo: /usr/lib/libstdc++.so.6: no version information available (required by hugo)
    hugo: /usr/lib/libstdc++.so.6: no version information available (required by hugo)

The build succeeded with the above message.  To suppress it,
[a switch to another CI runner][switch] will do.  The associated GitLab CI
config file was copied from [Hugo]'s documentation with
[some customizations for Hugo sample sites][hugo_eg]

[Hugo]: https://gohugo.io/
[Introduction]: https://themes.gohugo.io/theme/hugo-theme-introduction/
[GitHub]: https://github.com/
[GitLab]: https://gitlab.com/
[err]: https://gitlab.com/VincentTam/introduction/-/jobs/135854407
[switch]: https://gitlab.com/VincentTam/introduction/-/jobs/136398450
[hugo_eg]: https://gitlab.com/VincentTam/introduction/commit/4106d736e046d8e798034e9619b163e80adc67e5

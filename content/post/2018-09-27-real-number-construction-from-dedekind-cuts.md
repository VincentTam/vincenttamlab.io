---
title: Real Number Construction From Dedekind Cuts
subtitle: A geometrically intuitive approach
date: 2018-09-27T20:48:40+02:00
type: post
categories:
- math
tags:
- set theory
draft: false
---

### Goal

To gain a _real_ understanding on _real numbers_.

### Analytical construction

I "swallowed" the [Compleness Axiom][cplt_axiom], then I worked on exercises on
$\sup$ and $\inf$, and then the
[$\epsilon$-$\delta$ criterion for limits][lim_def], before completing $\Q$ with
[Cauchy sequences][cauchy].

I've also heard about the [completion of a metric space][cplt_ms] in a more
general setting.  My professor once said that it suffices to view this proof
once throughout lifetime: the proof itself _wasn't_ very useful.

The basic arithmetic properties of $\R$, as an equivalence class of Cauchy
sequences sharing the same limits, _didn't_ arouse our interests.  That's just
an extension of its rational counterpart due to some arithmetic properties of
limits.

### Algebraic construction

In my opinion, the construction of $\R$ from Dedekind cut is much more
_elegant_.

> $C \subseteq \Q$ is a Dedekind cut if
>
> 1. $C$ is a _proper, nonempty_ subset of $\Q$.
> 2. $C$ has _no_ maximum.
> 3. $C$ "goes left": $\forall a \in C, b \in \Q$ with $b < a$, $b \in C$.
>
> A _real number_ is defined to be a Dedekind cut.

This is a _proper_ definition since it _doesn't_ mention $\R$ at all.  This is
algebraically unnatural at the first time, but it captures the idea of cutting a
point on a line into two halves.  Some authors prefer to include the complement
of a Dedekind cut by writing $(C,D)$ so that $C \sqcup D = \Q$, but I'll omit
that to save ink.

Since the basic arithmetic operations are _far_ from my math studies in
probabilies and statistics, I'll just stop here and move to product
$\sigma$-algebras the next time.

[cplt_axiom]: https://en.wikipedia.org/wiki/Completeness_of_the_real_numbers
[lim_def]: https://en.wikipedia.org/wiki/Limit_of_a_sequence#Formal_definition
[cauchy]: https://en.wikipedia.org/wiki/Cauchy_sequence
[cplt_ms]: https://en.wikipedia.org/wiki/Complete_metric_space#Completion

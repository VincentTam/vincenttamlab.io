---
title: "Deploy Hugo Theme exampleSite With Github Actions"
date: 2021-03-06T13:39:05+01:00
categories:
- blogging
tags:
- GitHub Actions
- Hugo
---

### Motivation

To test PRs on the upstream of a [Hugo theme][themes] by setting up a testing
branch.

### Goal

To deploy a forked GitHub repo for a Hugo theme with `exampleSite` to GitHub
Pages using [GitHub Actions][actions].

The whole article is based on [my fork of Hugo Future Imperfect Slim][repo].

### References

1. [GitHub Actions for Hugo][ga4hugo]
2. A [Stack Overflow question showing `pwd` in GitHub Actions][gaShSub]
3. A [Hugo Discourse post about testing `exampleSite`][hgForum]

### Difficulties

I had failed for about ten times before I got the job done.

1. Generating a theme's `exampleSite` is quite tricky due to the difference in
file structure.  I used to preview that by

    ```sh
    cd exampleSite
    hugo server --themesDir=../..
    ```

    Running `hugo server --themesDir .. --source exampleSite` _doesn't_ work:
    `..` has to be replaced with `~` so that the command works—that _doesn't_
    work well on remote hosts when the current working path varies from host to
    host.

2. As a result, I tried to navigate to the parent directory with `${$(pwd)%/*}`.
That worked well on local Linux shells, but I ran into a
[bad substitution error][badSub].  Luckily I found the above linked Stack
Overflow question useful.
3. I got lost by switching between [GitHub Actions for GitHub Pages][ga4GH] and
[GitHub Actions for Hugo][ga4hugo].  With the idea that
[GitLab CI/CD can be migrated to GitHub Actions][mig], I confused GitLab CI/CD's
`pages.only` with GitHub Actions' `on.push.branches`.  I jumped to the
precautions for first deployment a few times and I mistakenly thought that the
source code was on the `gh-pages` branch.  In fact, GitHub Actions takes a
branch *not* named as `gh-pages`, and generates the static site on `gh-pages`.
The [error][sameBr] message `"You deploy from gh-pages to gh-pages"` was clear
enough, so I fixed this quickly.
4. After that, GitHub Actions showed a green tick, indicating that the deploy
was successful.  I configured my repo to use `gh-pages` for GitHub Pages.
However, when I navigated to the corresponding GitHub Pages, I got a 404 error.
`gh-pages` had one single empty file: `.nojekyll`.  It took me an hour to
discover [this line on the runner][pathErr]:

    ```
    cp: no such file or directory: /home/runner/work/hugo-future-imperfect-slim/hugo
    -future-imperfect-slim/public/*
    ```

    I then navigated up to find the command used for the build.

    ```sh
    hugo --themesDir "${PWD%/*}" --source exampleSite --minify
    ```

    I adapted this to my Zsh, and I found that the output was placed under
    `exampleSite/public/`.  After the correction of `publish_dir` in GitHub
    Actions config, everything works.

[themes]: https://themes.gohugo.io/
[actions]: https://github.com/features/actions
[repo]: https://github.com/VincentTam/hugo-future-imperfect-slim
[ga4hugo]: https://github.com/peaceiris/actions-hugo
[gaShSub]: https://stackoverflow.com/q/59383624/3184351
[hgForum]: https://discourse.gohugo.io/t/how-do-i-do-theme-development-with-an-example-site-in-the-repo/2136/10
[badSub]: https://github.com/VincentTam/hugo-future-imperfect-slim/runs/2046009042?check_suite_focus=true#step:4:4
[ga4GH]: https://github.com/peaceiris/actions-gh-pages
[mig]: https://docs.github.com/en/actions/learn-github-actions/migrating-from-gitlab-cicd-to-github-actions
[sameBr]: https://github.com/VincentTam/hugo-future-imperfect-slim/runs/2046060666?check_suite_focus=true#step:5:34
[pathErr]: https://github.com/VincentTam/hugo-future-imperfect-slim/runs/2046137955?check_suite_focus=true#step:5:45

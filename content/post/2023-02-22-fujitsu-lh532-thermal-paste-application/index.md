---
title: "Fujitsu LH532 Thermal Paste Application"
date: 2023-02-22T16:40:10+01:00
categories:
- technical support
tags:
- Fujitsu Lifebook
- LH532
- hardware
draft: false
---

### Background

My old laptop was bought ten years ago.  I had never changed the thermal paste
between its CPU and its fan.  As a result, it had been getting hot during my
online tutorials.

### Photos

{{< beautifulfigure src="IMG_20230218_103931066.jpg"
  title="thermal paste on CPU displaced after ten years"
  alt="thermal paste on CPU" >}}

{{< beautifulfigure src="IMG_20230218_103956343.jpg"
  title="thermal paste on fan displaced after ten years"
  alt="thermal paste on fan" >}}

{{< beautifulfigure src="IMG_20230218_104903563.jpg"
  title="dried thermal paste cleaned and removed"
  caption="with isopropyl and kitchen towel"
  alt="CPU and fan cleaned" >}}

I had to look at a [previous article][pp] written five years ago to confirm the
part that I wanted to disconnect.

{{< beautifulfigure src="IMG_20230218_105302468.jpg"
  title="Baraf Aerocool thermal paste"
  alt="Baraf Aerocool thermal paste" >}}

It has been imported by a German company, and it seems that its supplier is
Taiwanese.

The sticker for M\$ Win\* product ID has been damaged.

{{< beautifulfigure src="IMG_20230218_105530360.jpg"
  title="New thermal paste applied"
  alt="thermal paste application" >}}

I wanted to apply a little, then use a card to spread it out, but it came out
too fast.

{{< youtube kPtdZ2MtR8s >}}

{{< beautifulfigure src="IMG_20230218_111241708.jpg"
  title="Screws restored"
  caption="put the screws into the holes before tightening them"
  alt="Fujitsu LH532 inside" >}}

[pp]: /post/2018-07-04-fujitsu-lh532-fan-cleaning/

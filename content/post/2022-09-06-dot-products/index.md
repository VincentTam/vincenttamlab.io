---
title: "Dot Products"
subtitle: "My complicated derivation"
date: 2022-09-06T11:39:46+02:00
categories:
- math
tags:
- vectors
- linear algebra
draft: false
---

### Preface

There's a simpler derivation using

1. the geometric defintion
2. linearity from the definition
3. orthogonality of the canonical basic vectors $\vec{i}$, $\vec{j}$ and $\vec{k}$.

If I had read that, I wouldn't have type this document in LaTeX.  The following
content was transcribed from a PDF file that I made three days ago.

### Content

Recall: $\sum_{i=m}^{n} a_{i}=a_{m}+a_{m+1}+a_{m+2}+\cdots +a_{n-1}+a_{n},$
where $m$, $n$ are integers.

Example: $\sum_{i=3}^6 i^2 = 3^2 + 4^2 + 5^2 + 6^2 = 86$

Settings: let ${\color{red}{\vec{a} = \overrightarrow{OP_1} = (a_1, a_2, a_3) =
a_1 \vec{i} + a_2 \vec{j} + a_3 \vec{k}}}$ and
${\color{blue}{\vec{b} = \overrightarrow{OP_2} = (b_1, b_2, b_3) =
b_1 \vec{i} + b_2 \vec{j} + b_3 \vec{k} }}$ be two 3D-vectors, and
$\gamma = \angle {\color{red}{P_1}}O{\color{blue}{P_2}}$.

{{< beautifulfigure src="3d2v.svg" title="Two vectors in 3D-space"
caption="Two vectors in 3D-space" alt="Two vectors in 3D-space" >}}

<div class="boxedmath">
\begin{align}
{\color{red}{\vec{a}}} \cdot {\color{blue}{\vec{b}}} &=
\sum_{i=1}^3 {\color{red}{a_i}} {\color{blue}{b_i}} \tag{alg} \\
{\color{red}{\vec{a}}} \cdot {\color{blue}{\vec{b}}} &=
|{\color{red}{\vec{a}}}| \, |{\color{blue}{\vec{b}}}| \,
\cos {\color{mygreen}{\gamma}} \tag{geom}
\end{align}
</div>

WLOG assume $\boxed{|{\color{red}{\vec{a}}}| = |{\color{blue}{\vec{b}}}| = 1}$,
where $|{\color{red}{\vec{a}}}| = \sqrt{\sum_{i=1}^3 {\color{red}{a_i}}^2}$.

<div>
\[
  \left| \overrightarrow{{\color{red}{P_1}}{\color{blue}{P_2}}} \right|
  = \sqrt{\sum_{i=1}^3 ({\color{red}{a_i}} - {\color{blue}{b_i}})^2}
  = \sqrt{2\left(1 - \boxed{\sum_{i=1}^3 {\color{red}{a_i}}
    {\color{blue}{b_i}}} \right)}
\]
</div>

{{< beautifulfigure src="3d2vtopview.svg"
title="Top view of two vectors in 3D-space"
caption="Top view of two vectors in 3D-space"
alt="Top view of two vectors in 3D-space" >}}

Rotate $\angle {\color{red}{P_1}}O{\color{blue}{P_2}}$ so that the image
$\angle {\color{red}{P_1'}}O{\color{blue}{P_2}}'$ lies on the
$xy$-plane and ${\color{red}{\overrightarrow{OP_1'}}}$ coincides with the
positive $x$-axis.

<div>
\begin{align}
  \left| \overrightarrow{{\color{red}{P_1'}}{\color{blue}{P_2'}}} \right|
  &= \sqrt{({\color{red}{1}}-{\color{blue}{\cos\gamma}})^2 +
    ({\color{red}{0}}-{\color{blue}{\sin\gamma}})^2} \\
  &= \sqrt{1 - 2\cos\gamma + \cos^2\gamma + \sin^2\gamma} \\
  &= \sqrt{2(1 - \boxed{\cos\gamma})}
\end{align}
</div>

Since rotation doesn't change angle size and side lengths,
$\left| \overrightarrow{{\color{red}{P_1}}{\color{blue}{P_2}}} \right|
=\left| \overrightarrow{{\color{red}{P_1'}}{\color{blue}{P_2'}}} \right|$, so
the special case $\boxed{|{\color{red}{\vec{a}}}|=|{\color{blue}{\vec{b}}}|=1}$
is done.

Exercise: prove the general case.

### LaTeX skills used

I don't know why `\begin{align*}` doesn't work despite the surrounding
`<div>` tags.

The LaTeX analog of CSS float to the right is a wrap figure.

```tex
\usepackage{wrapfig}
...
\begin{wrapfigure}{r}{0.3\textwidth}
	\begin{tikzpicture}[scale=2.5,math3d]
	\end{tikzpicture}
\end{wrapfigure}
```

I forgot how to define color without importing an external package.

```tex
\documentclass[12pt]{article}
\usepackage[dvipsnames]{xcolor}
\definecolor{mygreen}{RGB}{0,96,0}
```

With used in `standalone` documents, the `dvipsnames` option has to be global.

```tex
\documentclass[tikz,dvipsnames]{standalone}
\usepackage{xcolor}
\definecolor{mygreen}{RGB}{0,96,0}
```

The `math3d` config is copied from the book [*TikZ pour
l'impatient*][tikz_book].

```tex
\tikzset{math3d/.style={x={(-0.353cm,-0.353cm)},z={(0cm,1cm)},y={(1cm,0cm)}}}
```

I tried to keep things within one page for printing purposes.  I omitted the
date and my name in favor of a simple title done quickly.

```tex
\begin{center}
  {\Huge Dot product}
\end{center}
```

I took away the paragraph indent `\parindent` and I reduced the vertical spacing
between adjacent lines.

```tex
\setlength{\parindent}{0pt}
\setlength{\lineskip}{1.1ex}
```

### Ti*k*Z source code

First figure

```tex
\documentclass[tikz,dvipsnames]{standalone}
\usepackage{xcolor}
\definecolor{mygreen}{RGB}{0,96,0}
\usetikzlibrary{calc}
\tikzset{math3d/.style=
{x= {(-0.353cm,-0.353cm)}, z={(0cm,1cm)},y={(1cm,0cm)}}}
\begin{document}
\begin{tikzpicture}[scale=2.5,math3d]
  \coordinate[label=left:$O$] (O) at (0,0,0);
  \coordinate[label=right:${\color{red}{P_1}}$] (P1) at (1.2,1,0.5);
  \coordinate[label=above:${\color{blue}{P_2}}$] (P2) at (-0.8,0.9,0.7);
  \draw[-latex] (O)--(1,0,0) node[pos=1.1]{$\vec{i}$};
  \draw[-latex] (O)--(0,1,0) node[pos=1.1]{$\vec{j}$};
  \draw[-latex] (O)--(0,0,1) node[pos=1.1]{$\vec{k}$};
  
  \draw[-latex,red] (O) -- (P1) node[midway,sloped,below] {$\vec{a}$};
  \draw[-latex,blue] (O) -- (P2) node[midway,sloped,above] {$\vec{b}$};
  
  \begin{scope}
    \clip (O) -- (P1) -- (P2) -- cycle;
    \draw[mygreen] (O) circle [x radius=0.4, y radius=0.2];
  \end{scope}
  \draw ($ (O)!.25!($ (P1)!.5!(P2) $) $) node [right,mygreen]{$\gamma$};
\end{tikzpicture}
\end{document}
```

Second picture

```tex
\documentclass[tikz,dvipsnames]{standalone}
\usepackage{xcolor}
\definecolor{mygreen}{RGB}{0,96,0}
\begin{document}
\begin{tikzpicture}[scale=1.5]
  \coordinate [label=below left:$O$] (O) at (0,0);
  \coordinate [label=below right:${\color{red}{P_1'=(1,0)}}$] (P1d) at (1,0);
  \coordinate [label=above right:${\color{blue}{P_2'=(\cos\gamma,\sin\gamma)}}$] (P2d) at (67:1);
  \draw[-latex] (-1.5,0) -- (1.5,0) node[pos=1.1] {$x$};
  \draw[-latex] (0,-1.5) -- (0,1.5) node[pos=1.1] {$y$};
  \draw[-latex,red] (O) -- (P1d);
  \draw[-latex,blue] (O) -- (P2d);
  \draw[->,mygreen] (0.3,0) arc (0:67:0.3) node[midway,sloped,above] {$\gamma$};
\end{tikzpicture}
\end{document}
```

[tikz_book]: http://math.et.info.free.fr/TikZ/index.html

---
title: Staticman Lab New Logos
date: 2018-11-21T08:31:23+01:00
type: post
categories:
- blogging
tags:
- Staticman
- Hugo
- GitLab
draft: false
---

{{< beautifulfigure src="/img/staticmanlab.svg" alt="StaticmanLab new logo" title="StaticmanLab's new logo" caption="GitLab logo recreated from Wikimedia's logo by Darby under CC-BY-SA 4.0 and Staticman logo on GitHub by Erlen Masson under MIT.">}}

The old icon for [Staticman Lab][StaticmanLab] was made by GIMP from Staticman's
icon in PNG in the GitHub repo.  Recently, I've found the SVG version of this
icon.  To serve customers better, I've recreated the logo from this SVG file so
that the edges in the logo become sharper.

This logo uses warm colors found in GitLab's logo.  Inkscape's eyedropper tool
(invoked by `d`) acts like GIMP's color picker.

I've added a <span class="red sigma-down">symbolic math representation of
Markdown in red</span>.  This symbolizes the aim of my technical setup and
writings over the past few months: to write _math_ in _Markdown_ freely.  The
<span class="red">red color</span> helps the <span class="red sigma-down">
"Markdown symbol"</span> to stand out from Staticman and GitLab logos.  To
import a $\LaTeX$ $\large\Sigma$ instead of a plain text Σ to Inkscape, it has
to be imported as paths instead of texts.  I've treated the down arrow ↓ in the
same way.

{{< highlight tex >}}
\documentclass[preview]{standalone}
\begin{document}
$\downarrow$
\end{document}
{{< /highlight >}}

To add the "H" of Hugo's logo, I've used Inkscape's clip set tool, which selects
the intersection of the object and the selector.  Its variant transformation
leaves the selector visible after the clip.

{{< beautifulfigure src="/img/staticmanlab-hugo.svg" alt="StaticmanLab's demo site new logo" title="StaticmanLab's demo site new logo" caption="GitLab logo recreated from Wikimedia's logo by Darby under CC-BY-SA 4.0 and Staticman logo on GitHub by Erlen Masson under MIT.">}}

To improve this favicon of this blog, I've to improve this favicon of this blog,
I've recreated this with GIMP since it's _impossible_ to export SVG to ICO from
Inkscape.  If the canvas is too *small*, the icon
![site favicon](/img/globe16.ico) will be unclear and blurred.  If the canvas is
too *big*, the icon ![site favicon](/img/globe16.ico) will be unclear and
blurred.  If it's too _big_, the file size will be *huge*.  At last, the one
with 64px × 64px works best.  That's the first time that I applied automatic
crop in GIMP to finish a real job.

[StaticmanLab]: https://gitlab.com/staticmanlab

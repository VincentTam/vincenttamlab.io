---
title: Merge GitHub Pull Requests
date: 2018-11-18T01:05:42+01:00
type: post
categories:
- technical support
tags:
- Git
- Staticman
draft: false
---

### Aim

To merge a pull request.

### How?

Let's take [Staticman PR 231][1] as an example.  I would like to test it before
[commiting this merget to Heroku][2].

    $ cd ~/staticman
    $ git branch -a
    * deploy
      dev
      master
      ...
    $ git remote -v
    eduardoboucas https://github.com/eduardoboucas/staticman.git (fetch)
    eduardoboucas https://github.com/eduardoboucas/staticman.git (push)
    heroku  https://git.heroku.com/staticman3.git (fetch)
    heroku  https://git.heroku.com/staticman3.git (push)
    ...
    $ git pull eduardoboucas pull/231/head:deploy
    remote: Enumerating objects: 10, done.
    remote: Counting objects: 100% (10/10), done.
    remote: Total 18 (delta 10), reused 10 (delta 10), pack-reused 8
    Unpacking objects: 100% (18/18), done.
    From https://github.com/eduardoboucas/staticman
     ! [rejected]        refs/pull/231/head -> deploy  (non-fast-forward)

I executed the last command on branch `dev`.  I _didn't_ have time to figure out
the reason for this error.  The following commands should work.

    $ git fetch eduardoboucas pull/231/head
    From https://github.com/eduardoboucas/staticman
     * branch            refs/pull/231/head -> FETCH_HEAD

    $ git merge FETCH_HEAD
    Merge made by the 'recursive' strategy.
     controllers/auth.js                |   6 ++---
     coverage/cobertura-coverage.xml    |  32 +++++++++++------------
     test/unit/controllers/auth.test.js | 127 ++++++++++++++++++++++++++++++-----------------------------------------------------------
     3 files changed, 61 insertions(+), 104 deletions(-)

A merge commit has two parents.  The second is the one merged from *another*
branch, and it's referred as `HEAD^2`.

To see the _overall_ change of the merged branch, a more useful command is
`git diff HEAD^..HEAD --stat`.

After some local testing (which I _didn't_ perform), I pushed this merge commit
so as to update the [my testing Staticman v3 API][3].

[1]: https://github.com/eduardoboucas/staticman/pull/231
[2]: /post/2018-09-16-staticman-powered-gitlab-pages/2/
[3]: https://staticman3.herokuapp.com/

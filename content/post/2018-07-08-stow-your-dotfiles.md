---
title: "Stow Your Dotfiles"
subtitle: "SSH config files managed by Git and GNU Stow"
date: 2018-07-08T16:01:22+02:00
type: post
categories:
- technical support
tags:
- dotfiles
- GNU Stow
- Git
- SSH
---

I correctly set up the system configuration file manager [GNU Stow][1]
and the version control system (VCS) [Git][2] on
[my Fujitsu Lifebook][3] so that the former can automatically
[install system configuration files like modules][4], whereas the later
can track the code in those files.

What I mean for *correctly* is that in the `ssh` folder of the remote
dotfiles repo, there is *neither* SSH key files *nor* entries
representing SSH keys in the gitignore file.  You may view
[Jan Uhlik's dotfiles][5] on GitLab as an example.

However, when I set up dotfiles on
[my newly upgraded Linux Mint 19][6], [Git][2] kept reminding me to
track my SSH keys.  Since I've *never* encountered such situation
before, I considered my setup *suboptimal*: even though I could ignore
this message, I found my SSH keys appearing in the list of untracked
files a bit annoying.  They drew my attention away from the *truely*
untracked files.

Having recalled myself the installation process of my dotfiles
modules, I _reinstalled my SSH files **after** the creation of the
folder_ `~/.ssh` so that this path represents *a folder* instead of a
symbolic link.

[1]: https://www.gnu.org/software/stow/
[2]: https://git-scm.com/
[3]: /post/2018-07-04-fujitsu-lh532-fan-cleaning/
[4]: https://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html
[5]: https://gitlab.com/juhlik/dotfiles/tree/e45cf51c3a073f64b1f707ac6cbc78ef38715e45
[6]: /post/2018-07-07-upgraded-to-linux-mint-19/

---
title: "Tikz to SVG in VS Code"
subtitle: "My little LaTeX Workshop recipe"
date: 2022-09-06T23:24:28+02:00
categories:
- technical support
tags:
- VS Code
- LaTeX Workshop
draft: false
---

### Goal

View SVG graphics generated from Ti*k*Z code in VS Code.

{{< beautifulfigure src="goal.png"
title="Compile TikZ code and preview SVG in VS Code"
caption="with LaTeX Workship and SVG Preview"
alt="LaTeX TikZ coding + SVG preview in VS Code" >}}

### Part I: generate SVG using LaTeX Workshop

From the IDE's parameters tab, I found the tools and recipes parameters.  I
clicked my user settings `settings.json` twice: once through the tools, and once
through the recipes.  Each click generated an array of default recipes or tools.
Each of the former is a sequence of the later, which represents a command.  The
official examples and placeholders are easy to follow.  Here's the two JSON
objects that I added.

```json
"latex-workshop.latex.recipes": [
    {
        "name": "SVG (latex ➞ dvisvgm)",
        "tools": [
            "latexmk_rconly",
            "dvisvgm"
        ]
    }
],
"latex-workshop.latex.tools": [
    {
        "name": "dvisvgm",
        "command": "dvisvgm",
        "args": [
            "--font-format=woff",
            "-b 10",
            "-d 2",
            "-R",
            "-o %DOC%.svg",
            "%DOC%"
        ],
        "env": {}
    }
],
```

- latexmk is a tool that automatically resolves the compilation dependencies and
execute LaTeX and Bib(La)TeX the right number of times.  With the `-pdf` option,
it compiles to PDF instead of DVI.
- the "RC" in `latexmk_rconly` means "release candidate".  It's different from
["RC" in VIMRC/bashrc][vimrc_meaning].
- the `%DOC%` placeholder means the source file's full path without the
extension name.

### Part II: preview the SVG within VS Code

The default background color is grey.  Imagine having a picture with black *x*
and *y*-axes.  Black objects with grey background don't look nice.

Luckily, in the VS Code Marketplace page of
[this SVG preview plugin][svg_preview], I found my desired settings with a
screenshot above.

```json
{
  "svgPreview.style": {
    "html": {
      "background": "white"
    }
  }
}
```

I copied that and pasted it below the settings in the previous section in the
same JSON file, and it worked as shown in the above screenshot.

[vimrc_meaning]: https://tech.serhatteker.com/post/2020-03/what-does-rc-mean/
[svg_preview]: https://marketplace.visualstudio.com/items?itemName=SimonSiefke.svg-preview
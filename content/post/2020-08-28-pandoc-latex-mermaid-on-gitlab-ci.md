---
title: "Pandoc LaTeX Mermaid on GitLab CI"
date: 2020-08-28T09:26:29+02:00
categories:
- technical support
tags:
- GitLab
- pandoc
- LaTeX
- mermaid
draft: false
---

### Goal

To provide an open-source alternative to the Docker image
[escalope/pandoc-mermaid-plantuml][escalope], whose `Dockerfile` isn't
available.

The rationale behind these difficult setup is simple: construct informative
[Mermaid] diagram with intuitive [Markdown] syntax in an open-source and
economic way.

This [newly constructed Docker image][docker-img] is entirely on GitLab.  No
Docker Hub account is needed.  For sample usage, consult `.gitlab-ci.yml` in
[my test project][test_pandoc].

### Difficulties

1. issues [raghur/mermaid-filter\#51][mf51] and [\#52][mf52]
2. issue [gitlab-org/gitlab-runner\#4566][gr4566]

### Useful code/articles

1. [time-machine-project/requests-for-comments@`470b0c5`][470b0c5] `Dockerfile`
2. [*Reduce Docker Image Sizes Using Alpine*][red-img-size]
3. [*Best practices for building docker images with GitLab CI*][gitlab-bld-img-blog]
with [the accompanying gist][gitlab-bld-img-gist]
4. The code block in the highlighted comment in item 2 of the above section
5. [pandoc installation for Docker][pandoc-doc]
6. Sample `Dockerfile` for Alpine Linux in the [troubleshooting of Puppeteer][pup]
7. [sc250024/docker-mermaid-cli@`3c9ddb5`][3c9ddb5] `src/puppeteerConfigFile.json`
8. [raghur/mermaid-filter][mf] project README's section about Puppeteer config
file

[escalope]: https://hub.docker.com/r/escalope/pandoc-mermaid-plantuml
[Mermaid]: https://mermaid-js.github.io/
[Markdown]: https://daringfireball.net/projects/markdown
[docker-img]: https://gitlab.com/VincentTam/pandoc-mermaid-docker/container_registry
[mf51]: https://github.com/raghur/mermaid-filter/issues/51
[mf52]: https://github.com/raghur/mermaid-filter/issues/52
[gr4566]: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4566#note_199261985
[test_pandoc]: https://gitlab.com/VincentTam/test_pandoc
[470b0c5]: https://github.com/time-machine-project/requests-for-comments/blob/470b0c5fdb8c1375ab8726c86b453cb9945e4be2/build/Dockerfile
[red-img-size]: https://sandtable.com/reduce-docker-image-sizes-using-alpine/
[gitlab-bld-img-blog]: https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/
[gitlab-bld-img-gist]: https://gist.github.com/florentchauveau/2dc4da0299d42258606fc3b0e148fc07
[pandoc-doc]: https://pandoc.org/installing.html#docker
[pup]: https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#running-on-alpine
[3c9ddb5]: https://github.com/sc250024/docker-mermaid-cli/blob/3c9ddb5fa7f240bbc96488674ae506ec091735d8/src/puppeteerConfigFile.json
[mf]: https://github.com/raghur/mermaid-filter

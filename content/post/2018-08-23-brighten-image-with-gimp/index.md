---
title: "Brighten Image With GIMP"
date: 2018-08-23T13:36:44+02:00
type: post
categories:
- blogging
tags:
- Hugo
- GIMP
- image optimization
---

### Motivation

One loves adding colors to his/her life by adding stunning photos to his/her
social media profile.  However, due to various constraints, such as weather,
budget and time, it's possible that the photos taken look dark and somber.

{{< beautifulfigure src="st-martine.jpg" title="Eglise St Martine" caption="Photo taken at Pont-du-Château, Puy-de-Dôme (63) on 30th June, 2017" alt="Eglise St Martine à Pont-du-Château" >}}

Despite a suboptimal choice of camera angle, a right tool enables you to take
the best out of the photo.  Let's go [GIMP][1]ing.

### Image shrinking

When the canvas size (of mobile devices) is much smaller than the photo width, the photo can be shrinked so as to save bandwidth.  To minimize the impact of the shrinking process on the image quality, one may use either the **Sinc** interpolation in GIMP 2.8, or the **NoHalo** method in GIMP 2.10.

<i class="fa fa-info-circle" aria-hidden></i> The former has been [replaced][2]
by the later during the development of GIMP 2.10.

### Operate on LAB colors

#### Rationale

Among the color spaces, LAB has the [largest color gamut][3].  This provides the _widest range of colors_.

Channel | Color components
------- | -----------------
L | <i class="fa fa-square"></i>--<i class="far fa-square"></i>
A | <i class="fa fa-square green"></i>--<i class="fa fa-square red" aria-hidden></i>
B | <i class="fa fa-square blue"></i>--<i class="fa fa-square yellow" aria-hidden></i>

The 'L' stands for "lightness" or "luminosity" <i class="far fa-lightbulb"
aria-hidden></i>.

#### Decomposition

This trick boosts the contrast of the photo <i class="far fa-image"
aria-hidden></i>.

1. **Color** &rarr; **Components** &rarr; **Decompose**
2. Select **LAB** for **Color model** in the drop-down list.
3. To select a specific channel, click on the right layer in the popup window.

##### L channel

- GIMP 2.8: You may apply the legacy Unsharp Mask (**Filters** &rarr;
  **Enhance** &rarr; **Unsharp Mask**) on the image.
- GIMP 2.10: The above process is _simplified_ thanks to [G'MIC][4]'s
  [Octave Sharpening][5] (start from a scale of 6).

Three parameters controlling the sharpening process:

- unsharp radius (2--10): a white boundary will be created if it's set too high.
  The greater the value, the sharper the image.
- amount (0.4--1.2): control the amount of contrast.  The greater the value, the
  sharper the image.
- threshold (0/9): the minimum contrast for the sharpening to take effect.  The
  _smaller_ the value, the sharper the image.

<i class="fa fa-info-circle" aria-hidden></i> The numbers in the brackets are
_for reference_ only.  Pleas preview the effect and adjust them to your own
needs.

Some [Adobe Photoshop][7] users prefer [smaller values][8] for the above
parameters.

##### A,B channels

If the photos <i class="far fa-image" aria-hidden></i> is _a bit but not too
dark_, [steepening the color curve][6] using **Colors** &rarr; **Curves** can be
a great technique.

<i class="fa fa-info-circle" aria-hidden></i> Make sure the _color curve passes
through the center of the graph_ to avoid unwanted color distortion.

#### Recomposition

1. **Color** &rarr; **Components** &rarr; **Recompose**
2. Change back to original window.

### Shift color levels

For photos taken in dark conditions, shifting the color levels using **Colors**
&rarr; **Levels** can help.  In the graph of input levels, the three pointers <i
class="fa fa-caret-up" aria-hidden></i> can be adjusted to rescale the
histogram.

Ref: [GIMP's doc section 5.7][9]

### Barber's Tricks

I followed [his steps on TurboFuture][10] to get the photo below.

{{< beautifulfigure src="st-martine-bright.jpg" title="Eglise St Martine Again" caption="Photo enhanced in GIMP one year later" alt="Eglise St Martine à Pont-du-Château" >}}

The *cheap* trick works so quick and easy to apply. This becomes my *valuable*
skill.

[1]: https://www.gimp.org
[2]: https://www.reddit.com/r/GIMP/comments/8j8lna/where_did_lanczos_3_sinc_scale_go_in_210_i_use/
[3]: https://en.wikipedia.org/wiki/CIELAB_color_space#Advantages
[4]: https://gmic.eu/
[5]: https://sourceforge.net/projects/gimpoctavesharp/
[6]: https://helpx.adobe.com/photoshop/using/curves-adjustment.html
[7]: https://www.adobe.com/products/photoshop.html
[8]: https://web.archive.org/web/20180808064525/http://www.digital-photo-secrets.com/tip/1507/photoshop-how-to-correctly-sharpen-an-image/
[9]: https://docs.gimp.org/en/gimp-tool-levels.html
[10]: https://turbofuture.com/graphic-design-video/How-to-brighten-up-dark-photos-without-blowing-highlights-using-FREE-software

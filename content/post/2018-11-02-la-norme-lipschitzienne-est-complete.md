---
title: "La norme lipschitzienne est complète"
date: 2018-11-02T23:50:57+01:00
type: post
categories:
- math
tags:
- real analysis
draft: false
---

Dans [l'article de Robert Fortet et Edith Mourier en 1953][1], une distance
entre deux mesures de probabilité sur un espace métrique est définie.

De nos jours, je trouve la façon dont ils l'ont écrit assez difficile à
comprendre.  Je suis plus à l'aise avec $\sup$ que "b.s." que désigne "borne
supérieure".  Ils se sont servi de $M[f]$ pour $\lVert f \rVert_{\rm Lip}$, où

$$
\lVert f \rVert_{\rm Lip} = \sup_{x \ne y} \frac{|f(x) - f(y)|}{d(x, y)}.
$$

Je me suis demandé la raison pour laquelle cette dernière application positive
était une *semi*-norme au lieu d'une vraie norme.  En effet, ils l'ont traitée
comme une vraie norme en *identifiant deux applications différant d'une
constante comme identiques*.

Cette identification joue un rôle important dans la preuve de la complétude de
la norme lipschitzienne.  On peut supposer, sans perte de généralité, qu'il
existe un point $x_0 \in S$ ($\mathscr{X}$ dans l'article) tel que $f_n(x_0)$
est nul pour tout $n \in \N$.

C'est classique qu'on commence par une suite $$(f_n)$$ de Cauchy dont on établit
une limite simple $$f_\infty$$.  Le paragraphe dernier permet l'égalité
suivante.

<div>
$$
\begin{aligned}
|f_{n+k}(x)-f_n(x)| &= |(f_{n+k}(x)-f_n(x))-(f_{n+k}(x_0)-f_n(x_0))| \\
&\le \lVert f_{n+k} - f_n \rVert_{\rm Lip} \; d(x,x_0)
\end{aligned}
$$
</div>

En faisant $n \to \infty$, et on a $$f_n \to f_\infty$$.

Soit $\epsilon > 0$.  Il existe $N \in \N$ tel que $\forall n \ge N, \forall k
\in \N$, $$\lVert f_{n+k} - f_n \rVert_{\rm Lip} < \epsilon$$.

<div>
$$
\forall n \ge N, \forall k \in \N, \forall x \ne y,
\frac{|(f_{n+k}(x)-f_n(x)) - (f_{n+k}(y)-f_n(y))|}{d(x, y)} < \epsilon
$$
</div>

Faisons $k \to \infty$.

<div>
$$
\forall n \ge N, \forall x \ne y,
\frac{|(f_\infty(x)-f_n(x)) - (f_\infty(y)-f_n(y))|}{d(x, y)} \le \epsilon
$$
</div>

Donc $$\forall n \ge N, \lVert f_\infty-f_n \rVert_{\rm Lip} \le \epsilon$$, ce
qui achève la preuve.

[1]: https://web.archive.org/web/20200805155814/http://archive.numdam.org/article/ASENS_1953_3_70_3_267_0.pdf

---
title: "Cantonese vs Mandarin for Reading Tang Poems"
subtitle: "Comparison using Du Mu's poem"
date: 2019-04-17T15:12:31+02:00
categories:
- languages
tags:
- poems
- Cantonese
- Mandarin
draft: false
---

This is a linkblog to a [post on Discuss HK][1] which claims how Mandarin (普通話)
messes up the flat and oblique tones (平仄) of the regulate verse (近體詩) below.

> 《山行》　杜牧  
> 遠上寒山石徑斜，白雲生處有人家。  
> 停車坐愛楓林晚，霜葉紅於二月花。

| Cantonese (粵語) | Mandarin (普通話) |
| --- | --- |
| 仄仄平平仄仄平 <br> 仄平平仄仄平平 <br> 平平仄仄平平仄 <br> 仄仄平平仄仄平 | 仄仄平平<em>平</em>仄平 <br> <em>平</em>平平仄仄平平 <br> 平平仄仄平平仄 <br> 仄仄平平仄仄平 |

However, Kimery has checked the Mandarin pinyin (拼音) and (s)he *doesn't* found
such problem except for some characters with *entering tone (入聲)*.  Luckily,
such characters are located at the *odd numbered position of a verse*.
Eventually, (after considering other factors,) that *won't* harm the variation
of tones *in principle*.

Zephyrli has enlightened us by elaborating on how the pedagogy of Mandarin's
tones (平仄) in recent years confuses future learners of classical Chinese poems.
According to him, the new principle of Mandarin's tones *isn't* based on any
ancient systems, so an appeal to the correspondance

| # | name | tone (平仄) |
| --- | --- | --- |
| 1 | 陰平 | 平 |
| 2 | 陽平 | 平 |
| 3 | 上 | 仄 |
| 4 | 去 | 仄 |

is *red herring* when comparing the tones (平仄) in Chinese spoken variants.

[1]: https://www.discuss.com.hk/viewthread.php?tid=11881519&page=1#pid246420659

---
title: "Existence of Four Triangle Centers"
subtitle: "A vector proof"
date: 2022-09-18T12:14:55+02:00
categories:
- math
tags:
- geometry
- vectors
draft: false
---

### Settings

Let

- $P$ be an arbitrary reference point.
- $\triangle ABC$ be a triangle.
- $\vec{a} = \overrightarrow{PA}$, $\vec{b} = \overrightarrow{PB}$,
$\vec{c} = \overrightarrow{PC}$

Remark: $O$ is reserved for circumcenter.

| symbol | name | meaning |
| :----: | :--- | :------ |
| $G$ | centroid | center of **g**ravity |
| $H$ | orthocenter | three "**h**eights" are concurrent |
| $I$ | incenter | center of **i**nscribed circle |
| $O$ | circumcenter | center of circumscribed circle |

### Centroid

Verify that $(\vec{a} + \vec{b} + \vec{c})/3$ satisfy the constraints.

### Orthocenter

Let

- $H$ be the point of intersection of two altitudes $AA_H$ and $BB_H$
- $\vec{h} = \overrightarrow{PH}$

<div>
\begin{align}
(\vec{h} - \vec{a}) \cdot (\vec{b} - \vec{c}) &= 0 \\
(\vec{h} - \vec{b}) \cdot (\vec{c} - \vec{a}) &= 0
\end{align}
</div>

Add these two equations together.

<div>
\begin{align}
\vec{h} \cdot (\vec{b} - \vec{a}) - \vec{a} \cdot (\vec{b} - \vec{c})
- \vec{b} \cdot (\vec{c} - \vec{a}) &= 0 \\
\vec{h} \cdot (\vec{b} - \vec{a}) - \vec{c} \cdot (\vec{b} - \vec{a}) &= 0 \\
(\vec{h} - \vec{c}) \cdot (\vec{b} - \vec{a}) &= 0
\end{align}
</div>

### Circumcenter

The skill is similar.  There's no need to actually solve for the vector of the
desired point in terms of $\vec{a}$, $\vec{b}$ and $\vec{c}$.

Let

- $O$ be the point of intersection of two perpendicular bisectors of sides
$\overrightarrow{AB}$ and $\overrightarrow{BC}$.
- $\vec{v} = \overrightarrow{PO}$

Remark: I used *v* instead of *o* above to avoid confusion with the little-O
notation.

<div>
\begin{align}
\left(\vec{v} - \frac{\vec{a}+\vec{b}}{2}\right) \cdot (\vec{a}-\vec{b}) &= 0 \\
\left(\vec{v} - \frac{\vec{b}+\vec{c}}{2}\right) \cdot (\vec{b}-\vec{c}) &= 0
\end{align}
</div>

Add these two equations together.

<div>
$$
\begin{align}
\vec{v}\cdot(\vec{a}-\vec{c}) - \frac{\normlr{\vec{a}}^2-\normlr{\vec{b}}^2}{2}
- \frac{\normlr{\vec{b}}^2-\normlr{\vec{c}}^2}{2} &= 0 \\
\vec{v}\cdot(\vec{a}-\vec{c}) - \frac{\normlr{\vec{a}}^2-\normlr{\vec{c}}^2}{2}
& = 0 \\
\left(\vec{v} - \frac{\vec{a}+\vec{c}}{2}\right) \cdot (\vec{a}-\vec{c}) &= 0
\end{align}
$$
</div>

### Incenter

Let $I$ be the intersection point of two angle bisectors $\overrightarrow{AD}$
and $\overrightarrow{BE}$.

I tried computing dot products two days ago, but I got something like

<div>
$$
2 \vec{v} \cdot \frac{\vec{b}-\vec{a}}{\normlr{\vec{b}-\vec{a}}}
- \frac{\normlr{\vec{b}}^2-\normlr{\vec{a}}^2}{\normlr{\vec{b}-\vec{a}}}
- \normlr{\vec{a} - \vec{c}} + \normlr{\vec{b} - \vec{c}},
$$
</div>

where $\vec{v} = \overrightarrow{PI}$, and I didn't know what to do next.

I have to combine two elementary formulae

1. an elementary fact about the angle bisector, which can be derived from
Co-Angle Theorem (共角定理)

    <div>$$\frac{\normlr{\overrightarrow{AB}}}{\normlr{\overrightarrow{AC}}} =
    \frac{\normlr{\overrightarrow{BD}}}{\normlr{\overrightarrow{DC}}}$$</div>

2. the convex combination of two vectors (定比分點公式)

    <div>$$\overrightarrow{AD} =
    \frac{\normlr{\overrightarrow{DC}}}{\normlr{\overrightarrow{BD}}+
    \normlr{\overrightarrow{DC}}} \, \overrightarrow{AB} +
    \frac{\normlr{\overrightarrow{BD}}}{\normlr{\overrightarrow{BD}}+
    \normlr{\overrightarrow{DC}}} \, \overrightarrow{AC}$$</div>

so that I know precisely where the incenter is.

<div>$$
\begin{align}
\overrightarrow{AD} &=
\frac{\normlr{\overrightarrow{AC}}}{\normlr{\overrightarrow{AB}}+
\normlr{\overrightarrow{AC}}} \, \overrightarrow{AB} +
\frac{\normlr{\overrightarrow{AB}}}{\normlr{\overrightarrow{AB}}+
\normlr{\overrightarrow{AC}}} \, \overrightarrow{AC} \\
\overrightarrow{BE} &=
\frac{\normlr{\overrightarrow{BC}}}{\normlr{\overrightarrow{BA}}+
\normlr{\overrightarrow{BC}}} \, \overrightarrow{BA} +
\frac{\normlr{\overrightarrow{BA}}}{\normlr{\overrightarrow{BA}}+
\normlr{\overrightarrow{BC}}} \, \overrightarrow{BC}
\end{align}
$$</div>

Let $s = \normlr{\overrightarrow{AI}}/\normlr{\overrightarrow{AD}}$ and
$t = \normlr{\overrightarrow{BI}}/\normlr{\overrightarrow{BE}}$.  Then
$\overrightarrow{AI} = s \overrightarrow{AB}$ and
$\overrightarrow{AI} = t \overrightarrow{AB}$.
We solve for $s$ and $t$ by considering $\overrightarrow{CA}+\overrightarrow{AI}
= \overrightarrow{CB} + \overrightarrow{BI} = \overrightarrow{CI}$.

<div>
$$\begin{align}
& \overrightarrow{CA} + \overrightarrow{AI} \\
=& \vec{a} - \vec{c} + s \left[\frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{a}}} \left(\vec{b} - \vec{a}\right)
+ \frac{\normlr{\vec{b} - \vec{a}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{a}}} \left(\vec{c} - \vec{a}\right)\right] \\
=& (1-s) \vec{a} + s \, \frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{a}}} \, \vec{b}
- \left(1 - s \, \frac{\normlr{\vec{b} - \vec{a}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{a}}}\right) \vec{c} \\
& \overrightarrow{CB} + \overrightarrow{BI} \\
=& \vec{b} - \vec{c} + t \left[\frac{\normlr{\vec{c} - \vec{b}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{b}}} \left(\vec{a} - \vec{b}\right) \\
+ \frac{\normlr{\vec{a} - \vec{b}}}{\normlr{\vec{c} - \vec{b}} + \normlr{\vec{a} - \vec{b}}} \left(\vec{c} - \vec{b}\right)\right] \\
=& t \, \frac{\normlr{\vec{c} - \vec{b}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{b}}} \, \vec{a} + (1-t) \, \vec{b}
- \left(1 - t \, \frac{\normlr{\vec{b} - \vec{a}}}{\normlr{\vec{b} - \vec{a}} + \normlr{\vec{c} - \vec{b}}}\right) \, \vec{c}
\end{align}$$
</div>

From the $\vec{c}$-component of $\overrightarrow{CI}$, we have
<div>$$
\frac{s}{\normlr{\vec{a} - \vec{b}} + \normlr{\vec{c} - \vec{a}}} =
\frac{t}{\normlr{\vec{a} - \vec{b}} + \normlr{\vec{b} - \vec{c}}}.
$$</div>

Substitute the above equation into the $\vec{b}$- component of
$\overrightarrow{CI}$.

<div>$$
\begin{align}
1 - t &= \normlr{\vec{c} - \vec{a}} \,
\frac{t}{\normlr{\vec{a} - \vec{b}} + \normlr{\vec{b} - \vec{c}}} \\
1 &= \left(1 + \frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}}}\right) t \\
t &= 1 - \frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}}
\end{align}
$$</div>

Substitute the above equation into the $\vec{a}$-component of
$\overrightarrow{CI}$.

<div>$$
s = 1 - \frac{\normlr{\vec{b} - \vec{c}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}}
$$</div>

Substitute the above two equations into $\overrightarrow{CI}$.

<div>$$
\begin{align}
& \overrightarrow{CI} \\
=& \frac{\normlr{\vec{b} - \vec{c}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}} \, \vec{a} +
\frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}} \, \vec{b} \\
& -\frac{\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}} \, \vec{c} \\
=& \frac{\normlr{\vec{b} - \vec{c}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}} \left(\vec{a} - \vec{c}\right) +
\frac{\normlr{\vec{c} - \vec{a}}}{\normlr{\vec{a} - \vec{b}} +
\normlr{\vec{b} - \vec{c}} + \normlr{\vec{c} - \vec{a}}} \left(\vec{b} - \vec{c}\right)
\end{align}
$$</div>

From the last equality, we see that
$\overrightarrow{CI} \parallel \overrightarrow{CF}$.

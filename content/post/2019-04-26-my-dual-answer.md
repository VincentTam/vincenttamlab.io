---
title: "My Dual Answer"
date: 2019-04-26T18:01:13+02:00
categories:
- math
tags:
- linear programming
- Math.SE
draft: false
---

> **Update**: The question has been _reopened_.

I intended to answer [김종현's problem on Math.SE][1].  However, the programs in
the question body _aren't_ typeset in [MathJax][2].  As a result, I downvoted
and closed this question because found it _unclear_.  From the proposed dual,
it seems that I _shouldn't_ interpret the primal as a linear program.  Anyways,
_without_ further clarifications from OP, I _found_ no reason to look at this
further.  Here's my intended answer:

> First, you have to properly write the primal as

<div>
$$
\begin{alignedat}{8}
\max \quad        & z = &   3w_1  & + & 4 w_2  & + & 5w_3 &         && \\
\text{s.t.} \quad &     & w_1 & - & w_2 & & & - & \varepsilon_1 & & & &  & \le 0 &&  \\
&     &     &   & w_2 & - & w_3 & & & - & \varepsilon_2 & & & \le 0 &&  \\
&     & & & & & w_3 & & & & & - & \varepsilon_3 & \le 0&& \\
& & & & & & & & 2\varepsilon_1 & + & 3\varepsilon_2 & + & 4\varepsilon_3 &\ge 1 && \\
& & w_1 & + & w_2 & + & w_3 & & & & & & & = 1, &&
\end{alignedat}
$$
</div>

> Your claimed dual
>
> <div>
> $$
> \begin{array}{ll}
> \max & \varepsilon_1 r_1 + \varepsilon_2 r_2 + \varepsilon_3 r_3 +
> (2\varepsilon_1 +3\varepsilon_2 + 4\varepsilon_3) r_4 + r_5 \\
> \text{s.t. } & x_1 + x_5 \le 3 \\
> & x_2 \le 4 \\
> & x_3 \le 5 \\
> & r_4, r_5 \text{ free}
> \end{array}
> $$
> </div>
>
> is incorrect since it's *not* a linear program.

[1]: https://math.stackexchange.com/q/3203415/290189
[2]: https://www.mathjax.org/

---
title: "Find Files for Rename"
date: 2018-12-23T11:57:58+01:00
type: post
categories:
- technical support
tags:
- Linux
draft: false
---

### Background

Three years ago, I wrote [*Used More Bash Utilities*][old] for batch renaming
files.  One of my [Facebook] friends has pointed out that it fails for file names containing whitespace.

### Problem

Recently, I would like to remove the extension name `.sh` of all shell scripts
in `~/bin`, so that `which {script}` will work *without* `.sh`.

### Method

A `for` loop over `$(ls)` is a basic solution, but `find` is _more correct_ since it's possible that a file name contains a space character.

```sh
find ~/bin -maxdepth 1 -name "*.sh" -type f -print0 |\
while read -d $'\0' file; do
  mv $file ${file%.*}
done
```

A list of matching files is piped to the `while` loop which iterates through
each item delimited by the null character `'\0'`.  The shell expansion
`${file%.*}` trim off the file extension.

[old]: https://vincenttam.github.io/blog/2015/08/22/used-more-bash-utilities/
[Facebook]: https://facebook.com

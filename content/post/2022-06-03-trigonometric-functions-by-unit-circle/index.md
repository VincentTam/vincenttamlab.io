---
title: "Trigonometric Functions by Unit Circle"
date: 2022-06-03T11:21:20+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

For secondary school students, I define cosine and sine as the *x* and
*y*-components of the point *A* (cos *θ*, sin *θ*) on the unit circle *x*² +
*y*² = 1, and the tangent function as the quotient of sine over cosine.

```tex
\begin{tikzpicture}[scale=3]
\coordinate (O) at (0,0);
\coordinate (H) at (0.6,0);
\coordinate (A) at (0.6,0.8);
\coordinate (E) at (1,0);
\coordinate (T) at (1,0.8/0.6);
\draw (O) circle (1);
\draw[->] (-1.3,0) -- (1.3,0) node [right]{$x$};
\draw[->] (0,-1.3) -- (0,1.3) node [above]{$y$};
\begin{scope}[thick]
\draw (O) node [below left] {$O$}
    -- (H) node [below right] {$H$}
    node [below, midway] {$\cos \theta$}
    -- (A) node [below, midway, sloped] {$\sin \theta$}
    node [above=5pt] {$A$}
    -- cycle node [above left, midway] {$1$};
\begin{scope}
    \clip (O) -- (A) -- (H) -- cycle;
    \draw (O) circle (0.1) node[right=7pt, above=5pt, anchor=west] {\small $\theta$};
\end{scope}
\draw (H) rectangle ++(-0.1,0.1);
\draw (E) rectangle ++(-0.1,0.1);
\draw (E) node [below right] {$E$}
    -- (T) node [below, midway, sloped] {$\tan \theta$}
    node [above, right] {$T$}
    -- (A);
\end{scope}
\end{tikzpicture}
```

![sine cosine unit circle](unit-circle.svg)

This definition allows us to retrieve the famous "sohcahtoa" mnemonics and some
elementary trigonometry identies like the Pythagorean identity and the
complementary angle identity sin(90° − *θ*) = cos *θ* for all real-valued *θ*.

To see the previous identity for non-acute *θ*, it suffices to swap the role of
*x* and *y*.  That means a reflection along the line *y* = *x* on the graph.
That might be a bit hard to imagine, so let's consider a simpler reflection
along the real line:

```
x---→ -----x----- ←---x
A          M          B
```

Points *A* and *B* are reflection of each other with respect to their midpoint
*M*.  One reflection flips the orientation of an arrow '→'/'←' once.

Getting back to the complementary angle identity, observe that the average of
*θ* and 90° − *θ* is 45°, and the sign of *θ* on both sides are different.
"cos(something)" represents the length of a horizontal line segment.  After a
reflection along *y* = *x*, the horizontal line segment becomes vertical.

| axis | *x* | *y* |
| -- | -- | -- |
| orientation | horizontal | vertical |
| neighbouring angle | *θ* | 90° − *θ* |
| chosen trigo funct | sin | cos |

Remarks: In the last row of the above table, "sin" and "cos" can be swapped.

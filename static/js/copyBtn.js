document.getElementsByTagName("article")[0].addEventListener("click", function(e) {
  if (e.target && e.target.nodeName == 'BUTTON') {
    doCopy(e.target.previousElementSibling.firstElementChild);
  }
});

function doCopy(target) {
  target.select();
  document.execCommand("copy");
}
